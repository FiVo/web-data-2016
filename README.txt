AUTHORS: Victor Nicolet, Finn Völkel

All comments that follow are assumed to be executed at 
the root of the project.
# ! IMPORTANT #
You need to rename the file webapp/webapp/keys_to_fill.py to
                            keys.py
If you plan to use any of the services relying on the keys
in keys.py, fill them in. So far the application doesn't
need any of the keys in it.

1.) Simple guide (for linux):
	Run once
		sh install.sh
	Run every time you want to launch the application
		sh run.sh
	Go to 3.)

2.) Long guide (for everything else):
Before starting the application please make sure you have 
the appropriate requirements:

	pip install -r requirements.txt

To set up the application:

	python webapp/manage.py makemigrations
	python webapp/manage.py migrate

To launch the application:

	python webapp/manage.py runserver

3.) To have the interface go to the url:
	
	127.0.0.1:8000/researchly/home/
