#!/bin/bash

# installing the requirements
sudo pip install -r requirements.txt

cp webapp/webapp/keys_to_fill.py webapp/webapp/keys.py

# setting up the application
python webapp/manage.py makemigrations
python webapp/manage.py migrate
