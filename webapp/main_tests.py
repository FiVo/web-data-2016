import random
from coreutils.authors import AuthorTests
from coreutils.papers import PublicationTest
from coreutils.text_mining import MinerTest
from coreutils.apis.arxive import ArxiveTests
from coreutils.title_generation import TitleGeneratorTest
from coreutils.wc_generator import WordCloudTester
from coreutils.authors import AuthorTests

def main():
    random.seed()
    tests = [
        #DblpTests(verbosity=1),
        #MSTests(),
        #AuthorTests(verbosity=1),
        #PublicationTest(verbosity=1),
        #RCSearchTest(),
        #GoogleSearchTest(verbosity=1)
        #RCSearchTest()
        #ArxiveTests(verbosity=1),
        #RCSearchTest(),
        #GoogleSearchTest(verbosity=1)
        #ArxiveTests(verbosity=1),
        MinerTest(),
        #WordCloudTester(),
        #TitleGeneratorTest()
    ]

    for test in tests:
        test.run()
        test.print_status()


if __name__ == '__main__':
    main()
