from django.contrib import admin

from models import SavedResearcher, SavedPublication, Coauthoring

# Register your models here.
admin.site.register(SavedPublication)
admin.site.register(SavedResearcher)
admin.site.register(Coauthoring)
