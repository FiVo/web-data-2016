from django.conf.urls import url

import views

urlpatterns = [
    url(r'^home/$', views.home, name="home"),
    url(r'^distance/$', views.distance, name="distance"),
    url(r'ajax/distance/$',
        views.compute_distance,
        name="compute_distance"),
    url(r'ajax/papers$',
        views.get_papers,
        name="get_papers"),
    url(r'ajax/wordcloud',
        views.research_intersection,
        name="wordcloud"),
    url(r'ajax/invented_titles',
        views.invented_titles,
        name="invented_titles"),
    url(r'^researcher/(?P<researcher_name>[\w-]+)',
        views.researcher,
        name="distance")
]
