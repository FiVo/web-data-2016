from django import forms
from django.forms import Form


class DistanceForm(Form):
    researcher1 = forms.CharField(max_length=200,
                                  label="First researcher name",
                                  initial="Pierre Senellart")
    researcher2 = forms.CharField(max_length=200,
                                  label="Second researcher name",
                                  initial="Serge Abiteboul")


class SingleResearcherForm(Form):
    researcher = forms.CharField(max_length=200,
                                 initial="Researcher name")
