import calendar
from BeautifulSoup import BeautifulSoup as soup

from django import template
from django.template.loader import render_to_string

from coreutils.authors import Researcher

register = template.Library()

# Researcher pretty printing
PRETTY_PROPERTY_NAME = {
    "name": u"Name",
    "firstName": u"First name",
    "lastName": u"Last name",
    "additionalName": u"Additional name",
    "address": u"Address",
    "affiliation": u"Affiliation",
    "alumniOf": u"Alumni of",
    "award": u"Award",
    "birth_info": u"Birth information",
    "death_info": u"Death information",
    "email": u"Email",
    "honoricPrefix": u"Honoric prefix",
    "honoricSuffix": u"Honoric suffix",
    "jobTItle": u"Job title",
    "memberOf": u"Member of",
    "nationality": u"Nationality",
    "workLocation": u"Work location",
    "sameAs": u"Urls"
}

OTHER_PROPERTIES = {
    "additionalType": "Additional type"
}


def property_contains_url(property):
    return property in ["sameAs"]


@register.simple_tag()
def pretty_researcher_info(dict_like):
    researcher_properties = dict_like["properties"]
    info = False
    # Researcher Name
    data = {"researcher__name": researcher_properties["name"]}
    # Affiliation to an organization
    if "affiliation" in researcher_properties:
        info = True
        affiliation = researcher_properties["affiliation"]
        if affiliation["type"] == u"http://schema.org/Organization":
            try:
                data["researcher_organization__name"] = affiliation["properties"]["name"]
                data["researcher_organization"] = True
            except KeyError:
                pass
    # Urls:
    if "sameAs" in researcher_properties:
        info = True
        if isinstance(researcher_properties["sameAs"], list):
            data["urls"] = [str(url) for url in researcher_properties["sameAs"]]
        else:
            data["urls"] = [str(researcher_properties["sameAs"])]

    if not info:
        data["not_much_info"] = True

    return render_to_string("extras/researcher.html", data)


@register.simple_tag(takes_context=True)
def pretty_paper(context, dict_like):
    info = dict_like["properties"]
    data = {"title": info["name"], "urls": []}
    data["type"] = "http://schema.org/ScholarlyArticle"
    if isinstance(info["authors"], list):
        data["authors"] = info["authors"]
    else:
        data["authors"] = [info["authors"]]

    if isinstance(info["sameAs"], list):
        for url in info["sameAs"]:
            data["urls"].append(url)
    else:
        data["urls"].append(info["sameAs"])
    if "description" in info:
        data["description"] = info["description"]
    if "pdf_link" in info:
        data["pdf_link"] = info["pdf_link"]
    # Date of publication
    if "year" in info:
        if "month" in info:
            data["date"] = calendar.month_abbr[info["month"]] + " " + str(info["year"])
        else:
            data["date"] = str(info["year"])
    if "additionalType" in info:
        data["additionalType"] = info["additionalType"]

    data["rname"] = [context["r1"].name, context["r2"].name]

    return render_to_string("extras/paper.html", data)


@register.simple_tag()
def render_suggestions(name, suggestion_list):
    return render_to_string("extras/suggestions.html", {
        "suggestions": suggestion_list,
        "name": name
    })


def test():
    d = Researcher("Pierre Senellart").get_defined_attributes()
    print soup(microdata_to_html(d))


# Simple dict to html translation of author information dicts
@register.simple_tag()
def microdata_to_html(value):
    if isinstance(value, list):
        if len(value) == 1:
            return microdata_to_html(value[0])
        else:
            s = u"\n<ul>\n"
            for v in value:
                s += u"<li>" + microdata_to_html(v) + u"</li>\n"
            s += u"</ul>\n"
            return s
    elif isinstance(value, dict):
        try:
            schema_type = value[u"type"]
        except KeyError:
            schema_type = value[u"additionalType"]

        wrapper = u"\n<ul class=\"collection with-header black-text\" itemscope itemtype=\"" + schema_type + u"\">\n"
        for propName, propValue in value[u"properties"].iteritems():
            if propName in PRETTY_PROPERTY_NAME:
                wrapper += u"<li class=\"collection-header\"><h4>" + PRETTY_PROPERTY_NAME[propName] + u"</h4></li>\n"
                wrapper += u"<li class=\"collection-item\" itemprop=\"" + propName + u"\">" + \
                           microdata_to_html(propValue) + \
                           u"</li>"

        return wrapper + u"</ul>\n"

    else:
        if not isinstance(value, str) or isinstance(value, unicode):
            return unicode(value)
        return value
