import os
import time
import urllib2
from threading import Thread

import slugify
from django.core.urlresolvers import reverse
from django.http.response import HttpResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt

from coreutils.apis.dblp import ResearcherQuery
from coreutils.authors import Researcher
from coreutils.text_mining import Miner
from coreutils.title_generation import TitleGenerator
from coreutils.wc_generator import WCgenerator
from forms import DistanceForm
from models import SavedResearcher

try:
    from webapp.webapp import settings
except ImportError:
    from webapp import settings

N_TITLES = 10
TIMEOUT = 60


def home(request):
    data = {
        "distance_form": DistanceForm()
    }
    return render(request, "coreui/home_form.html", data)


def distance(request):
    if request.method == 'POST':
        # Build the very simple two-researcher form
        form = DistanceForm(request.POST)

        if form.is_valid():
            # compute the distance between two researchers
            r1, r2 = None, None
            start = time.time()
            name1 = form.cleaned_data['researcher1']
            name2 = form.cleaned_data['researcher2']
            data = {}
            # Researcher are stored in the database when they're searched for
            # so we don't need to use DBLP each time
            researcher1 = SavedResearcher.get_by_name(name1)
            researcher2 = SavedResearcher.get_by_name(name2)
            if researcher1 is None:
                r1 = Researcher(name1)

                s1 = r1.not_found()
                data["r1_not_found"] = s1
                if s1:
                    data["suggestions1"] = r1.get_suggestions()
                    data["r1name"] = r1.name
                else:
                    researcher1 = SavedResearcher.save_researcher(r1)
            else:
                s1 = False

            if researcher2 is None:
                r2 = Researcher(name2)
                s2 = r2.not_found()
                data["r2_not_found"] = s2

                if s2:
                    data["suggestions2"] = r2.get_suggestions()
                    data["r2name"] = r2.name
                else:
                    researcher2 = SavedResearcher.save_researcher(r2)
            else:
                s2 = False

            if s1 or s2:
                data["distance_form"] = form
                data["got_suggestions"] = True
                return render(
                        request,
                        "coreui/home_form.html",
                        data
                )
            else:
                fname = slugify.slugify(researcher1.get_name()) + "_" + slugify.slugify(researcher2.get_name())
                print time.time() - start
                data["r1name"] = researcher1.get_name().encode(encoding="utf-8")
                data["r2name"] = researcher2.get_name()
                data["fname"] = fname
                print (time.time() - start)
                data["urlpt1"] = researcher1.url
                data["urlpt2"] = researcher2.url
                data["attrs1"] = researcher1.get_defined_attributes()
                data["attrs2"] = researcher2.get_defined_attributes()
                return render(
                        request,
                        "coreui/distance.html",
                        data
                )

    return redirect(reverse("home"))


def researcher(request, researcher_name, researcher_urlpt):
    researcher_query = ResearcherQuery(researcher_name, urlpt=researcher_urlpt)
    researcher = Researcher(researcher_name, researcher_query)


@csrf_exempt
def get_papers(request):
    if request.method != "POST":
        return render(request, "extras/errorcard.html",
                      {
                          "message": "Something's wrong with the code.."
                      })
    r1, r2 = researchers_out_of_request(request)
    fname = request.POST["fname"]
    papers1 = r1.get_papers()
    papers2 = r2.get_papers()
    BuildIntersectionThread(papers1, papers2, fname).start()
    coauthored_papers = []
    for p1 in papers1:
        for p2 in papers2:
            if p1 == p2:
                coauthored_papers.append(p1)
                papers1.remove(p1)
                papers2.remove(p2)

    if len(papers1) == 0:
        papers1 = None
    if len(papers2) == 0:
        papers2 = None
    if len(coauthored_papers) == 0:
        coauthored_papers = None
        coauthors = False
    else:
        coauthors = True

    print "Ready to render papers"
    return render(request, "coreui/papers_snippet.html",
                  {
                      "coauthors": coauthors,
                      "coauthored_papers": coauthored_papers,
                      "papers1": papers1,
                      "papers2": papers2,
                      "r1": r1,
                      "r2": r2
                  })


@csrf_exempt
def compute_distance(request):
    if request.method != "POST":
        return ""
    dist = "init"
    r1, r2 = researchers_out_of_request(request)

    try:
        dist = str(r1.get_distance_to(r2))
    except urllib2.HTTPError as e:
        if e.getcode() == 429:
            # print BaseHTTPRequestHandler.responses[e.getcode()][0]
            print "Too many requests"

    return HttpResponse(dist)


@csrf_exempt
def research_intersection(request):
    filename = request.POST["fname"]
    names = filename.split('_')
    filename2 = names[1] + "_" + names[0]
    start = time.time()
    cont = True
    while cont:
        cont = not (os.path.isfile(wc_path(filename)) or os.path.isfile(wc_path(filename2)))
        time.sleep(0.5)
        if time.time() - start > TIMEOUT:
            return render(request, "extras/errorcard.html",
                          {"message": "Timeout while waiting for research intersection result"}
                          )

    if os.path.isfile(wc_path(filename)):
        fname = filename
    else:
        fname = filename2

    return render(request,
                  "extras/research_intersection.html",
                  {"filename":
                       fname + ".png"}
                  )


@csrf_exempt
def invented_titles(request):
    filename = request.POST["fname"]
    names = filename.split('_')
    filename2 = names[1] + "_" + names[0]
    start = time.time()
    cont = True
    while cont:
        cont = not (os.path.isfile(titles_path(filename)) or os.path.isfile(titles_path(filename2)))
        time.sleep(0.5)
        if time.time() - start > TIMEOUT * 5:
            return render(request, "extras/errorcard.html",
                          {"message": "Timeout while waiting for research intersection result"}
                          )

    if os.path.isfile(titles_path(filename)):
        fname = filename
    else:
        fname = filename2
    # We just want the first 5 titles generated
    # TODO : need to rank the titles
    fd = open(titles_path(fname), "r")
    titles = fd.readlines()
    if len(titles) > 5:
        titles = titles[:5]
    return render(request,
                  "extras/invented_titles.html",
                  {"titles": titles}
                  )


def researcher_out_of_request(request):
    return Researcher(request.POST["name"], ResearcherQuery(request.POST["name"], urlpt=request.POST["url"]))


def researchers_out_of_request(request):
    name1 = request.POST["name1"]
    name2 = request.POST["name2"]
    r1 = Researcher(name1, ResearcherQuery(name1, urlpt=request.POST["url1"]))
    r2 = Researcher(name2, ResearcherQuery(name2, urlpt=request.POST["url2"]))
    return r1, r2


def wc_path(fname):
    return settings.BASE_DIR + "/coreutils/data/" + fname + ".png"


def titles_path(fname):
    return settings.BASE_DIR + "/coreutils/data/" + fname + ".txt"


class BuildIntersectionThread(Thread):
    """
    The research intersection of two authors is computed in the background while the page is loading.
    Currently, we store the results in simple files.
    TODO : build a better system to store the results
    """

    def __init__(self, papers1, papers2, fname):
        Thread.__init__(self)
        self.papers1 = papers1
        self.papers2 = papers2
        self.fname = fname

    def run(self):
        # If the wordcloud doesn't exist, build it.
        miner1 = None
        miner2 = None

        if not os.path.isfile(wc_path(self.fname)):
            miner1 = Miner(self.papers1, True)
            miner2 = Miner(self.papers2, True)
            wcg2 = WCgenerator(miner1.merge_frequency_dis_real(miner2))
            wcg2.generate(wc_path(self.fname))

        # Then generate titles if they haven't already been generated for these authors
        if not os.path.isfile(titles_path(self.fname)):
            print "Start generating titles"
            if miner1 is None:
                miner1 = Miner(self.papers1, True)
            if miner2 is None:
                miner2 = Miner(self.papers2, True)
            papers = self.papers1 + self.papers2
            title_generator = TitleGenerator(miner1.get_title_generation_data(miner2))
            titles = []
            for i in range(N_TITLES):
                titles.append(title_generator.generate())
            print "Write in file"
            fd = open(titles_path(self.fname), "w")
            fd.writelines("%s\n" % t for t in titles)
            fd.close()
