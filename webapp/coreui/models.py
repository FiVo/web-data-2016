import Queue
import json, ijson

from django.db import models

from coreutils.authors import Researcher
from coreutils.papers import Publication


# Create your models here.
class SavedResearcher(models.Model):
    name = models.CharField(max_length=200)
    attrs = models.TextField(null=True)
    url = models.CharField(unique=True, max_length=300)

    def get_defined_attributes(self):
        return json.loads(self.attrs)

    def get_name(self):
        return self.name

    @staticmethod
    def save_researcher(researcher):
        assert isinstance(researcher, Researcher)
        r = SavedResearcher(name=researcher.name,
                            attrs=json.dumps(researcher.get_defined_attributes()),
                            url=researcher.url)
        r.save()
        return r

    @staticmethod
    def get_by_name(researcher_name):
        try:
            return SavedResearcher.objects.all().get(name=researcher_name)
        except SavedResearcher.DoesNotExist:
            return None


class SavedPublication(models.Model):
    slug = models.CharField(max_length=500)
    name = models.TextField()
    json_dump = models.TextField()

    @staticmethod
    def save_publication(publication):
        assert isinstance(publication, Publication)
        p = SavedPublication(slug=publication.get_slugified_title(),
                             name=publication.get_title(),
                             json_dump=json.dumps(publication.get_schema()))


class Coauthoring(models.Model):
    author1 = models.CharField(max_length=200, db_index=True)
    author2 = models.CharField(max_length=200)

    @staticmethod
    def populate(json_file):
        f = open(json_file)
        first = None
        for prefix, theType, value in ijson.parse(f):
            if theType not in ['start_array','end_array','number']:
                if first is None:
                    first = value
                else:
                    Coauthoring(author1= first,author2 = value).save()
                    Coauthoring(author1= value,author2 = first).save()
                    first = None
        f.close()

    @staticmethod
    def get_coauthors(author_name):
        tuples = Coauthoring.objects.all().filter(author1=author_name)
        return [coauthor.author2 for coauthor in tuples]

    @staticmethod
    def distance(name1, name2):
        if name1 == name2:
            return 0
        q = Queue.Queue()
        q.put(name1)
        q.put(name2)
        name_dis = dict([(name1, (0, True)), (name2, (0, False))])
        while not q.empty():
            cur = q.get()
            cur_co = Coauthoring.get_coauthors(cur)

            for co_au in cur_co:
                if co_au in name_dis and name_dis[co_au][1] != name_dis[cur][1]:
                    return name_dis[co_au][0] + 1 + name_dis[cur][0]
                elif not (co_au in name_dis):
                    name_dis[co_au] = (name_dis[cur][0] + 1, name_dis[cur][1])
                    q.put(co_au)
