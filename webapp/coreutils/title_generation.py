# coding = UTF-8

import random
from collections import defaultdict
from nltk import FreqDist, pos_tag , word_tokenize
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import wordnet as WordNet
import language_check

#tests
import time
from text_mining import Miner
from tests import BaseTest
from authors import Researcher
from papers import Publication
from apis.arxive import ArxivQuery

# Found this blog helpful
# http://eli.thegreenplace.net/2010/01/28/generating-random-sentences-from-a-context-free-grammar/

class CFG(object):
    def __init__(self):
        self.prod = defaultdict(list)

    #essentially copied from the link above
    def add_prod(self, lhs, rhs):
        prods = rhs.split('|')
        for prod in prods:
            self.prod[lhs].append(tuple(prod.split()))

    def gen_symbol(self,symbol):
        sentence = ''
        rand_prod = random.choice(self.prod[symbol])

        for sym in rand_prod:
            if sym in self.prod:
                sentence += self.gen_symbol(sym)
            else:
                sentence += sym + ' '
        return sentence

class TitleGenerator(object):
    #NOUN_TAGS = ['NN','NNS']
    #VERB_TAGS = ['VB','VBD','VBG','VBN','VBP','VBZ']
    #ADJECTIVE_TAGS = ['JJ','JJS','JJR']
    #ADVERB_TAGS = ['RB','RBS','RBR']
    #FORBIDDEN = ['NNP', 'NNPS', 'SYM']

    def _correct_syntax(self,sentence):
        tool = language_check.LanguageTool('en-US')
        return tool.correct(sentence.lower())

    def __init__(self, title_generation_data):
        self.title_tags = title_generation_data[0]
        self.tag_word_dic = title_generation_data[1]
        self.tag_word_dic_fd = title_generation_data[2]

        #could happen in case both authors have 
        #adding default title does not change the problem as we 
        #as we art not sure to have the approriate words
        if self.title_tags == []:
            raise Exception("No title tags available")

    @staticmethod
    def check_duplicates(sentence):
        words = sentence.lower().split(' ')
        for w in words:
            if len(w) > 3 and words.count(w) > 1:
                return False
        return True

    # tries so that we do not run forever    
    def generate(self,tries=3):
        for i in range(tries):
            sentence = u''
            title_tags = random.choice(self.title_tags)
            for t in title_tags:
                if t in self.tag_word_dic_fd:
                    w = random.choice(self.tag_word_dic_fd[t])
                else:
                    w = random.choice(self.tag_word_dic[t])
                sentence += w + u' '
            sentence = self._correct_syntax(sentence)
            #TODO currently plural and singular not checked
            if self.check_duplicates(sentence):
                return sentence
        return sentence

class TitleGeneratorTest(BaseTest):

    def run(self):
        self.passed = self.test_duplicates_test()
        self.passed = self.passed and self.emtpy_title_tags_test()
        self.passed = self.passed and self.title_generation_test()
        self.runs += 1

    def test_duplicates_test(self):
        start = time.time()
        res = (TitleGenerator.check_duplicates("Hello Hello") is False)
        res = res and (TitleGenerator.check_duplicates("the the") is True)
        res = res and (TitleGenerator.check_duplicates("Hello my name is") is True)
        elapsed = (time.time() - start)
        print ">>>> Duplicate test in " + str(elapsed) + "s"
        return res

    def emtpy_title_tags_test(self):
        start = time.time()
        try:
            tg = TitleGenerator(([],None,None))
        except Exception as e:
            print e.args
        elapsed = (time.time() - start)
        print ">>>> Empty title tag test in " + str(elapsed) + "s"
        return True

    def title_generation_test(self):
        start = time.time()
        arxiv_query1 = ArxivQuery(author="Michel Habib")
        arxiv_query2 = ArxivQuery(author="Serge Abiteboul")
        elapsed = (time.time() - start)
        print ">>>> Initializing Researcher in " + str(elapsed) + "s"
        start = time.time()
        papers1 = Publication.from_arxiv(arxiv_query1)
        papers2 = Publication.from_arxiv(arxiv_query2)
        elapsed = (time.time() - start)
        print ">>>> Getting papers in " + str(elapsed) + "s"
        miner1 = Miner(papers1,True)
        miner2 = Miner(papers2,True)
        #papers1.extend(papers2)
        tg = TitleGenerator(miner1.get_title_generation_data(miner2))
        for i in range(20):
            print tg.generate()
        return True
