# coding = UTF-8

import json
import time
from datetime import datetime
from json.decoder import WHITESPACE

import microdata
from slugify import slugify

from apis.arxive import ARXIV_DATEFORMAT, ArxivQuery
from coreutils.apis.searchEngines.googleScholar import *
from tests import BaseTest
from utils import update_field, max_none, flatten_and_remove_duplicates, extract_links

PUBLICATION_ATTRIBUTES_TO_SCHEMA = {
    'type': 'additionalType',
    'mdate': 'dateModified',
    'title': 'name',
    'ee': 'sameAs',
    'isbn': 'sameAs',
    'url': 'sameAs',
    'publisher': 'publisher'
}


class Publication(object):
    # We initialize a Publication with a dblp query
    # ['type', 'sub_type', 'mdate',
    #  'authors', 'editors', 'title', 'year', 'month', 'journal',
    #  'volume', 'number', 'chapter', 'pages', 'ee', 'isbn', 'url',
    #  'booktitle', 'crossref', 'publisher', 'school', 'citations',
    #  'series']
    OBJECT_TYPE = "http://schema.org/ScholarlyArticle"

    def __init__(self, record_query=None, abstract=None):
        """
        A Publication can be initaliazed with different objects depending on the source,
        but this object must provide a doi is some way to identify the Publication
        :param record_query: a query object, like a dblp RecordQuery
        :param abstract: an Abstract object
        """
        self.key = None
        if record_query is not None:
            self.key = record_query.key
        self.exists = False
        self.data = None
        self.abstract = abstract
        # TODO do the same kind of hashmapping for elsevier
        if record_query is not None and record_query.load_data():
            self.data = record_query.data.copy()
            self.exists = True
        elif abstract is not None:
            self.exists = True
        else:
            self.exists = False

        # Articles loaded using Google Scholar
        self.articles = None

    # Private method : search google scholar for additional information,
    # especially a website
    def _google_scholar(self):
        querier = ScholarQuerier()
        settings = ScholarSettings()
        settings.set_citation_format(ScholarSettings.CITFORM_BIBTEX)
        querier.apply_settings(settings)

        query = SearchScholarQuery()

        query.set_words_some(self.data["title"])
        # Currently searches only for one author
        query.set_author(self.data["authors"][0])

        query.set_timeframe(start=int(self.data["year"]))

        # We search only using the title
        query.set_scope(True)

        querier.send_query(query)

        self.articles = querier.articles

    def _set_data(self, dict_object):
        if self.data is None:
            self.data = {}
        self.data.update(dict_object)

    @staticmethod
    def from_arxiv(arxiv_query, max_pubs=10):
        """
        Parses a result from arxiv, this result must be a dict built by parsing the feed from Arxiv' API.
        See api/arxiv.py
        :param arxiv_results:dict-like objects
        :return a list of Publications, one for each result of arxiv_results
        """
        publications = []
        arxiv_results = arxiv_query.get_current_page()
        if arxiv_results is None:
            raise PublicationException

        cont = True
        while len(publications) < max_pubs and cont:
            for result in arxiv_results:
                if ArxivQuery.is_cs_result(result):
                    abstract = Abstract(
                            doi=result.doi,
                            title=result.title,
                            abstract=result.summary,
                            pdf_link=result.pdf_url
                    )
                    publication = Publication(abstract=abstract)
                    publication._set_data({
                        'mdate': datetime.strftime(result.updated, ARXIV_DATEFORMAT),
                        'authors': result.authors,
                        'title': result.title,
                        'year': result.published.year,
                        'month': result.published.month,
                        'journal_reference': result.journal_reference,
                        'ee': result.doi,
                        'url': extract_links(result.links),
                    })
                    publication.exists = True
                    publications.append(publication)

            cont = arxiv_query.has_next_page()
            arxiv_results = arxiv_query.next_page()

        return publications

    def get_abstract(self):
        if self.exists and self.abstract is not None:
            return self.abstract.get_abstract()
        else:
            return None

    def get_schema(self):
        props = {}
        if self.data is not None:
            for k, v in self.data.iteritems():
                if v is not None:
                    values = []
                    if isinstance(v, list):
                        for value in v:
                            if isinstance(value, microdata.Item):
                                values.append(json.loads(value.json()))
                            else:
                                values.append(v)
                    else:
                        values = v

                    if k in PUBLICATION_ATTRIBUTES_TO_SCHEMA:
                        key = PUBLICATION_ATTRIBUTES_TO_SCHEMA[k]
                        if key in props:
                            props[key].append(flatten_and_remove_duplicates(values))
                        else:
                            props[key] = [flatten_and_remove_duplicates(values)]
                    else:
                        props[k] = v

        if self.abstract is not None:
            props["description"] = self.abstract.get_abstract()
            props["pdf_link"] = self.abstract.pdf_link
        props = flatten_and_remove_duplicates(props)

        json_repr = {"type": self.OBJECT_TYPE, "properties": props}
        # Add some info from object
        json_repr["key"] = self.key

        return json_repr

    def get_slugified_title(self):
        return slugify(unicode(self.get_title()))

    def get_title(self):
        if self.exists:
            if self.data is not None:
                return self.data['title']
            else:
                return self.abstract.get_title()
        else:
            return None

    def get_unique_identifier(self):
        return self.get_slugified_title()

    def merge(self, other):
        if self.key is not None and other.key is not None and self.key != other.key:
            print "Cannot merge these objects, they're not equal, merge aborted"
            print " Keys : %s and %s" % (self.key, other.key)
            print "Slugs : %s and %s" % (self.get_slugified_title(), other.get_slugified_title())
            return False
        update_field(self, other, "key")
        update_field(self, other, "exists")
        update_field(self, other, "data")
        if self.abstract is not None:
            self.abstract.merge(other.abstract)
        else:
            self.abstract = other.abstract
        return True

    def print_articles(self):
        if not self.articles:
            self._google_scholar()
        articles_txt(self.articles)

    def __str__(self):
        if self.exists:
            return "\n".join([
                "Publication " + self.get_title(),
                json.dumps(self.data, indent=2)
            ])
        else:
            return "Publication with title" + self.get_title() + " doesn't exist."

    def __eq__(self, other):
        # How do we know if two publications are equal ? This is a complex problem since
        # we build publications from various sources and we do not always have their DOI
        # for example.
        # TODO : build a better equality test between Publications
        # For now, we know we should have at least a title
        # TODO : thisequality test with coauthors has some bugs
        # sameauthors = True
        # other_data_copy = other.data.copy()
        # for author in self.data["authors"]:
        #     if author not in other_data_copy["authors"]:
        #         sameauthors = False
        #         other_data_copy["authors"].remove(author)
        #         pass
        # if sameauthors:
        #     sameauthors = (len(other_data_copy["authors"]) == 0)

        return self.get_slugified_title() == other.get_slugified_title()


class PublicationEncoder(json.JSONEncoder):
    def encode(self, o):
        assert isinstance(o, Publication)
        json_object = o.get_schema()
        return json.dumps(json_object)


# TODO : not impleemented !
class PublicationDecoder(json.JSONDecoder):
    def decode(self, s, _w=WHITESPACE.match):
        json_repr = json.loads()
        p = Publication()


class Abstract(object):
    def __init__(self, doi, title, abstract, pdf_link=None):
        self.doi = doi
        self.title = title
        self.abstract = abstract
        self.pdf_link = pdf_link

    def get_doi(self):
        return self.doi

    def get_title(self):
        return self.title

    def get_abstract(self):
        return self.abstract

    def merge(self, other):
        for attr, value in self.__dict__.iteritems():
            self.__setattr__(
                    attr,
                    max_none(
                            self.__getattribute__(attr),
                            other.__getattribute__(attr)
                    )
            )

    def __eq__(self, other):
        return self.title == other.title

    def __ne__(self, other):
        return self.title != other.title


class PublicationException(Exception):
    def __init__(self):
        super(PublicationException, self.__init__())


class PublicationTest(BaseTest):
    TEST_NAME = "Publications Test"

    def run(self):
        self.passed = self.from_arxiv_test()
        self.runs += 1

    @staticmethod
    def abstract_test():
        pub = Publication(Abstract("1234", "The Absolute Truth", "TotoTiti"))
        print "Title is :" + pub.get_title() + "\n"
        print "Abstract is :" + pub.get_abstract() + "\n"
        return True

    def from_arxiv_test(self):
        start = time.time()
        self.print_verb(">>>> Initialize and query arxiv for an author")
        arxiv_query = ArxivQuery(author="Pierre Senellart")
        results = arxiv_query.get_current_page()
        self.print_verb(">>>> Elapsed : %.3f seconds" % (time.time() - start))
        self.print_verb("Use the results to build Publication objects")
        publications = Publication.from_arxiv(results)
        self.print_verb("We have %d publications." % len(publications))
        for publication in publications:
            self.print_verb(publication.data["title"])
        if len(publications) > 0:
            return True
        else:
            return False
