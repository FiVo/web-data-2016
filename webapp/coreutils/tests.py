# coding=UTF-8


# Simple base class for Tests : when you implement any test class, just implement the run
# method. The run method should increment the runs value, and update the passed value.
# You can also provide a specific name for your test
class BaseTest(object):
    """
        A Basic test class. Derive this class in each class test, you just have to implement
        the tests end put them in the run() method, which must return set passed to True only
        if all tests pass.
        The run() method must also increment the runs class variable each time it is called and
        terminating.
    """
    TEST_NAME = "Base Test - does not test anything !"

    def __init__(self, verbosity=0):
        self.runs = 0
        self.passed = False
        self.verbosity = verbosity

    def run(self):
        """
        self.passed = Did the test pass ?
        self.runs += 1
        """
        pass

    def print_verb(self, *strings):
        """
        :param strings: string or a list of strings. strings[verbosity-1] will be
        printed (or the last string in th array if the verbosity is set too high
        but non-zero.
        :return: if verbosity is set to 0, prints nothing
        """
        if self.verbosity > 0:
            i = min(len(strings), self.verbosity) - 1
            print strings[i]

    def print_status(self):
        print "Status : " + self.TEST_NAME
        if self.runs == 0:
            print "You didn't run the test or the run failed at some point."
            return False
        else:
            print "Run " + str(self.runs) + " times."
            if self.passed:
                print "### Test passed."
            else:
                print "### Test failed. !!!"
            return self.passed
