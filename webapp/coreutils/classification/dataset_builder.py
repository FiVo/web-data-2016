# coding=UTF-8
import Queue

import coreutils.apis.searchEngines.duckduckgo as dgo
from coreutils.apis.searchEngines.googleSearch import CSResearcherSearch
from ..apis.customSearch import ResearcherCustomSearch
from ..authors import Researcher


def with_search():
    start = Researcher("Albert Cohen")
    visited = {}
    q = Queue.Queue()
    q.put(start)

    positives = open("researcher_websites.txt", "w")
    negatives = open("not_researcher_websites.txt", "w")
    while not q.empty():
        cur = q.get()
        for coauthor in cur.get_coauthors_from():
            if coauthor.name not in visited:
                q.put(coauthor)
        gs = CSResearcherSearch(cur.name).search()
        visited[cur.name] = "1"
        for title, res in gs._google_results.iteritems():
            print res['url']
            save = raw_input("Researcher homepage ? ")

            if save == 'y':
                positives.write(cur.name + ",")
                positives.write(res['url'].encode('utf-8'))
                positives.write("\n")
                # Go to next researcher, a researcher has only one homepage
                break
            elif save == 'n':
                negatives.write(cur.name + ",")
                negatives.write(res['url'].encode('utf-8'))
                negatives.write("\n")

            else:
                positives.close()
                negatives.close()
                exit(0)


def with_custom_search():
    start = Researcher("Albert Cohen")
    visited = {}
    q = Queue.Queue()
    q.put(start)

    positives = open("researcher_websites.txt", "w")
    negatives = open("not_researcher_websites.txt", "w")
    while not q.empty():
        cur = q.get()
        for coauthor in cur.get_coauthors_from():
            if coauthor.name not in visited:
                q.put(coauthor)
        gs = ResearcherCustomSearch(cur.name).load()
        visited[cur.name] = "1"
        for url in gs.get_urls():
            print url
            save = raw_input("Researcher homepage ? ")

            if save == 'y':
                positives.write(cur.name + ",")
                positives.write(url)
                positives.write("\n")
                # Go to next researcher, a researcher has only one homepage
                break
            elif save == 'n':
                negatives.write(cur.name + ",")
                negatives.write(url)
                negatives.write("\n")

            else:
                positives.close()
                negatives.close()
                exit(0)

def with_duckduckgo():
    start = Researcher("Albert Cohen")
    visited = {}
    q = Queue.Queue()
    q.put(start)

    positives = open("researcher_websites.txt", "w")
    negatives = open("not_researcher_websites.txt", "w")
    while not q.empty():
        cur = q.get()
        for coauthor in cur.get_coauthors_from():
            if coauthor.name not in visited:
                q.put(coauthor)
        gs = dgo.query(cur.name.encode("utf-8"))
        visited[cur.name] = "1"
        for res in gs.results:
            print res.url
            save = raw_input("Researcher homepage ? ")

            if save == 'y':
                positives.write(cur.name + ",")
                positives.write(res.url.encode("utf-8"))
                positives.write("\n")
                # Go to next researcher, a researcher has only one homepage
                break
            elif save == 'n':
                negatives.write(cur.name + ",")
                negatives.write(res.url.encode("utf-8"))
                negatives.write("\n")

            else:
                positives.close()
                negatives.close()
                exit(0)

def main():
    with_duckduckgo()

if __name__ == '__main__':
    main()