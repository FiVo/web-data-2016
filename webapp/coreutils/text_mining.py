# coding = UTF-8

import os
from collections import defaultdict
from papers import Publication
from nltk import FreqDist, pos_tag, word_tokenize
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer

#for tests
from tests import BaseTest
from authors import Researcher
from apis.elsevier import ResearcherQuery as RQ_elsevier
from apis.arxive import ARXIV_DATEFORMAT, ArxivQuery
import time

cached_stopwords = set(stopwords.words('english'))
most_common_words = set(line.strip() for line in \
        open(os.path.join(os.path.dirname(__file__),'data/google-1000-english.txt')))

class Miner(object):
    # Miner is an object that receives a list of Publications upon initialization
    # and strips them of all the unnecessary
    # currently uses all available information from a Publication
    # could be specialized to use only some

    FORBIDDEN = ['NNP', 'NNPS', 'SYM']

    def __init__(self,publis,common_words_removal=False, tag_abstracts=False, max_title_tag = 5):
        self.publis = publis
        # words stripped of stopwords (possibly common words)
        self.stripped = None
        # frequency distribution
        self.fd = None
        self.fd_real = None
        self.common_words_removal = common_words_removal
        self.lmtzr = WordNetLemmatizer()
        #words from titles
        self.tag_dic_normal = defaultdict(list)
        #words according tag words according frequency distribution
        self.tag_dic_fd = None
        # list of titles by tags 
        self.title_tags = []
        self.tag_abstracts = tag_abstracts
        self.max_title_tag = max_title_tag


    def _get_root(self,word):
        word_n = self.lmtzr.lemmatize(word,'n')
        word_v = self.lmtzr.lemmatize(word,'v')
        if word_n != word or word_v != word:
            if word_n != word:
                word = word_n
            else:
                word = word_v
        return word

    def _not_forbidden(self,l):
        for e in l:
            if e in self.FORBIDDEN or not e in self.tag_dic_normal:
                return False
        return True

    def get_stripped(self):
        if self.stripped is None:
            stripped = []
            dirty = []
            for publi in self.publis:
                #title
                title = publi.get_title()
                if not title is None:
                    tokens = word_tokenize(title.lower())
                    dirty.extend(tokens)
                    if len(self.title_tags) < self.max_title_tag:
                        tagged = pos_tag(tokens)
                        title_tags =[]
                        for w,t in tagged:
                            title_tags.append(t)
                            if w.isalpha():
                                self.tag_dic_normal[t].append(w)
                        if self._not_forbidden(title_tags):
                            self.title_tags.append(title_tags)
                #abstract
                abstract = publi.get_abstract()
                if not abstract is None:
                    tokens = word_tokenize(abstract.lower())
                    dirty.extend(tokens)
                    # takes really long otherwise
                    if self.tag_abstracts:
                        tagged = pos_tag(tokens)
                        for w,t in tagged:
                            if w.isalpha():
                                self.tag_dic_normal[t].append(w)
            for word in dirty:
                w = self._get_root(word)
                if w.isalpha() and len(w) > 2:
                    stripped.append(w)
            self.stripped = filter(lambda w: not w in cached_stopwords, stripped)
            if self.common_words_removal:
                self.stripped = filter(lambda w: not w in most_common_words, self.stripped)

        return self.stripped

    def _freq_distribution_discrete_to_real(self,frequency_dis):
        fd_real = []
        total = frequency_dis.N()
        for w,v in frequency_dis.iteritems():
            fd_real.append((w,(float(v)/float(total))))
        return sorted(fd_real,key=lambda x:x[1],reverse=True)

    def _renormalize(self,fd_real):
        res = []
        for w,f in fd_real:
            res.append((w,f/2.0))
        return sorted(res,key=lambda x:x[1],reverse=True)

    def get_frequency_dis(self):
        if self.fd is None:
            self.fd = FreqDist(self.get_stripped())
        return self.fd

    def get_frequency_dis_real(self):
        if self.fd_real is None:
            self.fd_real = self._freq_distribution_discrete_to_real(self.get_frequency_dis())
        return self.fd_real

    def merge_frequency_dis(self,miner2):
        fd2 = miner2.get_frequency_dis()
        res = self.get_frequency_dis().copy()
        for w,v in fd2.iteritems():
             res[w] += v
        return res

    # TODO do some tests if the real distribution is always well balanced as 
    # some the words from on researcher might outway the ones from the other
    def merge_frequency_dis_real(self,miner2):
        fd_real = self.get_frequency_dis_real()
        fd_real.extend(miner2.get_frequency_dis_real())
        return self._renormalize(fd_real)

    def get_title_tags(self):
        self.get_stripped()
        return self.title_tags

    def get_tag_dic_normal(self):
        self.get_stripped()
        return self.tag_dic_normal

    # Attention the tag_dic will be initialized with the nb of the first call
    def get_tag_dic_fd(self,nb = 100):
        if self.tag_dic_fd is None:
            if self.fd_real is None:
                self.get_frequency_dis_real()
            fd = [x[0] for x in self.fd_real[:nb]]
            self.tag_dic_fd = defaultdict(list)
            for t,wl in self.tag_dic_normal.items():
                for w in wl:
                    if self._get_root(w) in fd:
                        self.tag_dic_fd[t].append(w)
        return self.tag_dic_fd

    def get_title_generation_data(self,miner2):
        title_tags = self.get_title_tags()
        title_tags.extend(miner2.get_title_tags())
        merged_tag_dic_normal = self.merge_tag_dic_normal(miner2)
        merged_tag_dic_fd = self.merge_tag_dic_fd(miner2)
        return (title_tags,merged_tag_dic_normal,merged_tag_dic_fd)

    def _merge_tag_dics(self,dic1,dic2):
        res = dic1.copy()
        for t,l in dic2.items():
            res[t].extend(l)
        return res

    def merge_tag_dic_normal(self, miner2):
        return self._merge_tag_dics(self.get_tag_dic_normal(),miner2.get_tag_dic_normal())

    def merge_tag_dic_fd(self,miner2):
        return self._merge_tag_dics(self.get_tag_dic_fd(50),miner2.get_tag_dic_fd(50))

class MinerTest(BaseTest):
    #TODO check what happens if there is no access to Elsevier
    def run(self):
        self.passed = True
        #start = time.time()
        #self.passed = self.get_frequency_dis_test()
        #elapsed = (time.time() - start)
        #print ">>>>> Passed frequency test in " + str(elapsed) + " s"
        #elapsed = (time.time() - start)
        #start = time.time()
        #self.passed = self.get_frequency_dis_real_test()
        #elapsed = (time.time() - start)
        #print ">>>>> Passed frequency test real in " + str(elapsed) + " s"
        start = time.time()
        self.passed = self.passed and self.merge_frequency_dis_real_test()
        elapsed = (time.time() - start)
        print ">>>>> Passed merge frequency test real in " + str(elapsed) + " s"
        #start = time.time()
        #self.passed = self.passed and self.merge_frequency_dis_test_arxiv()
        #elapsed = (time.time() - start)
        #print ">>>>> Passed merge frequency arxiv test in " + str(elapsed) + " s"
        #start = time.time()
        #self.passed = self.passed and self.merge_frequency_dis_real_test_arxiv()
        #elapsed = (time.time() - start)
        #print ">>>>> Passed merge frequency distribution real arxiv test in " + str(elapsed) + " s"
        #start = time.time()
        #self.passed = self.passed and self.merge_tag_dic_test()
        #elapsed = (time.time() - start)
        #print ">>>>> Passed merge tag dictionary test in " + str(elapsed) + " s"
        #start = time.time()
        #self.passed = self.passed and self.title_generation_data_test()
        #elapsed = (time.time() - start)
        #print ">>>>> Passed title generation data test in " + str(elapsed) + " s"
        self.runs += 1
        return self.passed

    def get_frequency_dis_test(self):
        start = time.time()
        michel = Researcher("Michel Habib") #, RQ_elsevier("Michel Habib"))
        elapsed = (time.time() - start)
        print ">>>> Initializing Researcher in " + str(elapsed) + "s"
        start = time.time()
        papers = michel.get_papers()
        elapsed = (time.time() - start)
        print ">>>> Getting papers in " + str(elapsed) + "s"
        miner = Miner(papers)
        miner.get_frequency_dis()
        for word in miner.fd:
            print word, ':', miner.fd[word]
        return True

    def get_frequency_dis_real_test(self):
        start = time.time()
        arxiv_query_michel = ArxivQuery(author="Michel Habib")
        elapsed = (time.time() - start)
        print ">>>> Initializing Researcher in " + str(elapsed) + "s"
        start = time.time()
        papers = Publication.from_arxiv(arxiv_query_michel)
        elapsed = (time.time() - start)
        print ">>>> Getting papers in " + str(elapsed) + "s"
        miner = Miner(papers,True)
        fd_real = miner.get_frequency_dis_real()
        for word,f in fd_real:
            print word, ':', f
        return True

    def merge_frequency_dis_real_test(self):
        start = time.time()
        michel = Researcher("Alan M. Turing") #, RQ_elsevier("Michel Habib"))
        serge = Researcher("Serge Abiteboul") #, RQ_elsevier("Serge Abiteboul"))
        elapsed = (time.time() - start)
        print ">>>> Initializing Researchers in " + str(elapsed) + "s"
        start = time.time()
        papers1 = michel.get_papers()
        papers2 = serge.get_papers()
        elapsed = (time.time() - start)
        print ">>>> Getting papers in " + str(elapsed) + "s"
        miner1 = Miner(papers1,True)
        miner2 = Miner(papers2,True)
        merged = miner1.merge_frequency_dis_real(miner2)
        for word,f in merged:
            print word, ':', f
        return True

    def merge_frequency_dis_test_arxiv(self):
        start = time.time()
        arxiv_query1 = ArxivQuery(author="Pierre Senellart")
        arxiv_query2 = ArxivQuery(author="Michel Habib")
        elapsed = (time.time() - start)
        print ">>>> Initializing Researchers in " + str(elapsed) + "s"
        start = time.time()
        papers1 = Publication.from_arxiv(arxiv_query1)
        papers2 = Publication.from_arxiv(arxiv_query2)
        elapsed = (time.time() - start)
        print ">>>> Getting papers in " + str(elapsed) + "s"
        miner1 = Miner(papers1)
        miner2 = Miner(papers2)
        merged = miner1.merge_frequency_dis(miner2)
        for word in merged:
            print word, ':', merged[word]
        return True

    def merge_frequency_dis_real_test_arxiv(self):
        start = time.time()
        arxiv_query1 = ArxivQuery(author="Alan M. Turing")
        arxiv_query2 = ArxivQuery(author="Serge Abiteboul")
        elapsed = (time.time() - start)
        print ">>>> Initializing Researchers in " + str(elapsed) + "s"
        start = time.time()
        papers1 = Publication.from_arxiv(arxiv_query1)
        papers2 = Publication.from_arxiv(arxiv_query2)
        elapsed = (time.time() - start)
        print ">>>> Getting papers in " + str(elapsed) + "s"
        miner1 = Miner(papers1,True)
        miner2 = Miner(papers2,True)
        merged = miner1.merge_frequency_dis_real(miner2)
        for word,f in merged:
            print word, ':', f
        return True

    def merge_tag_dic_test(self):
        start = time.time()
        arxiv_query1 = ArxivQuery(author="Serge Abiteboul")
        arxiv_query2 = ArxivQuery(author="Michel Habib")
        elapsed = (time.time() - start)
        print ">>>> Initializing Researchers in " + str(elapsed) + "s"
        start = time.time()
        papers1 = Publication.from_arxiv(arxiv_query1)
        papers2 = Publication.from_arxiv(arxiv_query2)
        elapsed = (time.time() - start)
        print ">>>> Getting papers in " + str(elapsed) + "s"
        miner1 = Miner(papers1,True)
        miner2 = Miner(papers2,True)
        merged = miner1.merge_tag_dic_fd(miner2)
        for tag,word_list in merged.items():
            print tag, ':', word_list
        return True

    def title_generation_data_test(self):
        start = time.time()
        arxiv_query1 = ArxivQuery(author="Michel Habib")
        arxiv_query2 = ArxivQuery(author="Serge Abiteboul")
        elapsed = (time.time() - start)
        print ">>>> Initializing Researcher in " + str(elapsed) + "s"
        start = time.time()
        papers1 = Publication.from_arxiv(arxiv_query1)
        papers2 = Publication.from_arxiv(arxiv_query2)
        elapsed = (time.time() - start)
        print ">>>> Getting papers in " + str(elapsed) + "s"
        miner1 = Miner(papers1,True)
        miner2 = Miner(papers2,True)
        (title_tags,tag_dic, tag_dic_fd_merged) = miner1.get_title_generation_data(miner2)
        for t in title_tags:
            print t
        for t,wl in tag_dic.items():
            print t, ':', wl
        for t,wl in tag_dic_fd_merged.items():
            print t, ':', wl
        return True

