# coding = UTF-8
# !/usr/bin/python


def get_attr_or_none(dict_like_object, attr):
    if isinstance(dict_like_object, dict):
        if attr in dict_like_object:
            return dict_like_object[attr]
        else:
            return None
    else:
        return getattr(dict_like_object, attr, None)


def max_none(a, b):
    if a is None:
        return b
    else:
        return a


def merge_obj(v, w):
    if v is None:
        return w
    elif w is None:
        return v
    elif v == w:
        return v
    elif isinstance(v, dict) and isinstance(w, dict):
        return merge_dicts(v, w)
    elif isinstance(v, list):
        if isinstance(w, list):
            v.extend(w)
        else:
            v.append(w)
        return flatten_and_remove_duplicates(v)
    else:
        return v


def merge_dicts(a, a2):
    b = a2.copy()
    merge_res = {}
    for k, v in a.iteritems():
        if k in b:
            merge_res[k] = merge_obj(v, b[k])
            b.pop(k)
        else:
            merge_res[k] = v
    for k, v in b.iteritems():
        merge_res[k] = v

    return merge_res


def update_field(object1, object2, attr_name):
    if get_attr_or_none(object1, attr_name) is None:
        setattr(object1, attr_name, get_attr_or_none(object2, attr_name))
        return
    obj1_attr = getattr(object1, attr_name)
    obj2_attr = get_attr_or_none(object2, attr_name)

    if obj2_attr is not None:
        setattr(object1, attr_name, merge_obj(obj1_attr, obj2_attr))


def extract_links(link_list):
    """
    Extracts links from a list of links with information
    :param link_list: a list of dicts {"href": , "type", "rel"}
    :return: a list of strings (the urls)
    """
    if isinstance(link_list, list) and len(link_list) == 1:
        return extract_links(link_list[0])
    else:
        ll = []
        for rich_link in link_list:
            if isinstance(rich_link, dict):
                if "href" in rich_link and \
                                "type" in rich_link and rich_link["type"] == u"text/html":
                    ll.append(rich_link["href"])
        return ll


def flatten_and_remove_duplicates(obj):
    if isinstance(obj, dict):
        for k, v in obj.iteritems():
            obj[k] = flatten_and_remove_duplicates(v)
        return obj
    elif isinstance(obj, list):
        fv = []
        if obj.__len__() == 1:
            return flatten_and_remove_duplicates(obj[0])
        for v in obj:
            if isinstance(v, list):
                flatlist = flatten_and_remove_duplicates(v)
                for elt in flatlist:
                    flat_elt = flatten_and_remove_duplicates(elt)
                    if elt not in fv:
                        fv.append(elt)
            elif v not in fv:
                fv.append(v)
        return fv
    else:
        return obj


# Base error class with message

class Error(object):
    def __init__(self, message):
        if isinstance(message, unicode):
            self.message = message
        else:
            self.message = unicode(message, encoding='utf-8')

    def __str__(self):
        return self.message.decode('utf-8')

    def __unicode__(self):
        return self.message


class BadAPI(Error, Exception):
    def __str__(self):
        return u"Bad API : " + self.message.decode('utf-8')

    def __unicode__(self):
        return u"Bad API : " + self.message
