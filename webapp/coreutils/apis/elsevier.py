import urllib
import urllib2
from BaseHTTPServer import BaseHTTPRequestHandler
from lxml import etree

# in case of problems check out
# http://dev.elsevier.com/tecdoc_api_authentication.html
# for more information
# TODO this module is currently no error prune
# and can only be used at university
# 

# implement author search
# implement abstract retrieval
# implement article retrieval
from coreutils.papers import Abstract

ELSEVIER_BASE_URL = "http://api.elsevier.com/content/"


class ElsevierQuery(object):
    API_KEY = "ef2ed512e634d30fa54b06f2862b8775"
    BASE_URL = ELSEVIER_BASE_URL
    REQUEST_HEADERS = {"Accept": "application/xml", "X-ELS-APIKey": API_KEY}


class ResearcherQuery(ElsevierQuery):
    search_url = ''

    # if researche_name is ambigious, ie more than two names give as tuple
    def __init__(self, researcher_name, given_and_surname=None):
        self.access_check = False
        self.search_url = self.BASE_URL + "search/author?"
        if isinstance(researcher_name, unicode):
            self.query_suffix = researcher_name
        else:
            self.query_suffix = unicode(researcher_name, 'utf-8')

        if given_and_surname is None:
            self.givenname = researcher_name.split()[0]
            self.surname = researcher_name.split()[1]
        else:
            self.givenname = given_and_surname[0]
            self.surname = given_and_surname[1]
        self._suggestions_ = []

        try:
            self._is_matching_query = self._matches()
        except NoMatchException as ne:
            print ne.__unicode__()
            self._is_matching_query = False

    def check_access(self):
        return self.access_check

    def get_researcher_name(self):
        return self.givenname + " " + self.surname

    # private method called upon init to see if query matches
    def _matches(self):
        values = {'query': 'AUTHFIRST(%s) AND AUTHLASTNAME(%s)' % (self.givenname, self.surname)}
        data = urllib.urlencode(values)
        req = urllib2.Request(self.search_url + data, headers=self.REQUEST_HEADERS)
        try:
            doc = etree.parse(urllib2.urlopen(req))
        except urllib2.HTTPError as e:
            print BaseHTTPRequestHandler.responses[e.getcode()]
            # no access to resources
            if e.getcode() == 401:
                self.access_check = False
            return False

        self.access_check = True

        # TODO I didn't know how to do this properly as there are namespaces in the xml files which are
        # not visible when written to file. Maybe due to html5 
        surnames = doc.xpath(".//*[local-name() = 'preferred-name']//*[local-name() = 'surname']/text()")
        givennames = doc.xpath(".//*[local-name() = 'preferred-name']//*[local-name() = 'given-name']/text()")
        # author_ids = reduce((lambda l,Id: l.append(Id[10:]), l),doc.xpath(".//*[local-name() = 'identifier']/text()"),[])
        l = doc.xpath(".//*[local-name() = 'identifier']/text()")
        author_ids = []
        # TODO check for multiple matches
        for Id in l:
            author_ids.append(Id[10:])

        for i in range(len(surnames)):
            # print surnames[i] + " " + self.surname + "\n"
            # print givennames[i] + " " + self.givenname + "\n"
            self._suggestions_.append(givennames[i] + " " + surnames[i])
            if surnames[i] == self.surname and givennames[i] == self.givenname:
                self.author_id = author_ids[i]
                return True

        raise NoMatchException(self._suggestions_)

    # Public interface
    def matches(self):
        return self._is_matching_query

    def get_coauthors(self):
        if self.matches() and self.check_access():
            values = {'co-author': self.author_id}
            data = urllib.urlencode(values)
            req = urllib2.Request(self.search_url + data, headers=self.REQUEST_HEADERS)
            surnames = []
            givennames = []
            try:
                doc = etree.parse(urllib2.urlopen(req))
                surnames = doc.xpath(".//*[local-name() = 'preferred-name']//*[local-name() = 'surname']/text()")
                givennames = doc.xpath(".//*[local-name() = 'preferred-name']//*[local-name() = 'given-name']/text()")
                print etree.tostring(doc, pretty_print=True)
            except urllib2.HTTPError as e:
                BaseHTTPRequestHandler.responses[e.getcode()]


            result = []
            for i in range(len(surnames)):
                result.append(givennames[i] + " " + surnames[i])

            return result

        else:
            raise Exception("No access or no matching author")

    # returns nbAbstracts if possible 
    # otherwise max found
    def abstracts(self, nbAbstracts):
        if self.matches() and self.check_access():
            AUTHOR_URL = ELSEVIER_BASE_URL + "author/author_id/" + self.author_id
            req = urllib2.Request(AUTHOR_URL, headers=self.REQUEST_HEADERS)
            try:
                doc = etree.parse(urllib2.urlopen(req))
            except urllib2.HTTPError as e:
                BaseHTTPRequestHandler.responses[e.getcode()]

            author_search_url = doc.xpath(".//link[@rel = 'search']/@href")[0]
            req = urllib2.Request(author_search_url, headers=self.REQUEST_HEADERS)
            try:
                doc = etree.parse(urllib2.urlopen(req))
            except urllib2.HTTPError as e:
                BaseHTTPRequestHandler.responses[e.getcode()]

            # namespaces
            ns = {'ce': "http://www.elsevier.com/xml/ani/common",
                  'dc': "http://purl.org/dc/elements/1.1/",
                  'prism': "http://prismstandard.org/namespaces/basic/2.0/"}

            paper_urls = doc.xpath(".//*[local-name() = 'entry']//prism:url/text()", namespaces=ns)
            abstracts = []
            for i in range(min(nbAbstracts, len(paper_urls))):
                req = urllib2.Request(paper_urls[i], headers=self.REQUEST_HEADERS)
                try:
                    doc = etree.parse(urllib2.urlopen(req))
                except urllib2.HTTPError as e:
                    BaseHTTPRequestHandler.responses[e.getcode()]

                paras = doc.xpath(".//abstract//ce:para/text()", namespaces=ns)
                title = doc.xpath(".//dc:title/text()", namespaces=ns)
                doi = doc.xpath(".//prism:doi/text()", namespaces=ns)

                # TODO find workaround for this case
                if len(doi) == 0:
                    abstracts.append(Abstract(None, title[0], " ".join(paras)))
                else:
                    abstracts.append(Abstract(doi[0], title[0], " ".join(paras)))

            return abstracts
        else:
            raise Exception("No access or no matching author")

    def get_suggestions(self):
        return self._suggestions_


# Error classes
class NoMatchException(Exception):
    def __init__(self, suggestions):
        self.suggestions = suggestions

    def __unicode__(self):
        if self.suggestions.__len__() > 0:
            s = u"Query didn't match, but here are some similar names you can try : \n" + \
                unicode.join(u'\n', self.suggestions)
            return s
        else:
            return u"Query didn't match, and there is no suggestions !\n"


# some tests
class ElsevierTest(object):
    @staticmethod
    def test_query_coauthors():
        res = ResearcherQuery("Michel Habib").coauthors()
        for r in res:
            print r + "\n"
        return True

    @staticmethod
    def test_query_abstracts():
        abstracts = ResearcherQuery("Michel Habib").abstracts()
        for ab in abstracts:
            print ab
        return True

    @staticmethod
    def test_query_not_matching_with_suggestions():
        print "Test appproximate query"
        return ResearcherQuery("M. Habib").matches()
