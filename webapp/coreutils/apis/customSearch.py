from googleapiclient.discovery import build

try:
    import webapp.keys as keys
except ImportError:
    import webapp.webapp.keys as keys
from ..tests import BaseTest


class ResearcherCustomSearch(object):
    def __init__(self, researcher_name):
        self.researcher_name = researcher_name
        self.results = None

    def load(self, using_schema="Thing"):
        if using_schema not in keys.GOOGLE_CSES.keys():
            raise Exception(
                    "Cannot load CustomSearch, you must use one of the following keywords" +
                    " to specify a Google Search Custom Engine : \n" +
                    ", ".join(keys.GOOGLE_CSES.keys())
            )

        service = build("customsearch", "v1",
                        developerKey=keys.GOOGLE_SERVER_KEY)

        cx = keys.GOOGLE_CSES[using_schema]

        self.results = service.cse().list(
                q=self.researcher_name,
                cx=cx
        ).execute()

        return self

    def print_result_summary(self):
        for result in self.results[u'items']:
            print result[u'formattedUrl']

    def get_urls(self):
        return [res[u'formattedUrl'].encode('utf-8') for res in self.results[u'items']]


class RCSearchTest(BaseTest):
    TEST_NAME = "Researcher Custom Search Test"

    def run(self):
        self.passed = self.try_search_thing()
        self.runs += 1

    @staticmethod
    def try_search_thing():
        researcher_name = "Michel Habib"
        rcs = ResearcherCustomSearch(researcher_name)
        rcs.load()
        rcs.print_result_summary()
        return True
