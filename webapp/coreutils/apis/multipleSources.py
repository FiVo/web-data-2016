# coding = UTF-8
import re
import urllib2
from lxml import etree
from lxml.html import html5parser

from ..tests import BaseTest


class MultipleSourceObject(object):
    SUPPORTED_SOURCES = ""

    def __init__(self, url):
        self.url = url
        self.objects = {}


class MSAbstract(MultipleSourceObject):
    SUPPORTED_SOURCES = {
        "acm": {
            "xpath": "//x:*[@id=\"abstract\"]/x:div/x:div/x:p",
            "url_matcher": re.compile(
                    "http://dl\.acm\.org/citation\.cfm\?(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|"
                    "(?:%[0-9a-fA-F][0-9a-fA-F]))+")
        },
        "sciencedirect": {
            "xpath": "//x:*[@class=\"abstract svAbstract\"]/x:p",
            "url_matcher": re.compile(
                    "http://www\.sciencedirect\.com/science/article/\?(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|"
                    "(?:%[0-9a-fA-F][0-9a-fA-F]))+"
            )
        }
    }

    def __init__(self, url):
        super(MSAbstract, self).__init__(url)

    def load(self, dump_parts=False):
        for source_id, info in self.SUPPORTED_SOURCES.iteritems():
            if info["url_matcher"].match(self.url):
                # Loading url

                html = None
                try:
                    html = urllib2.urlopen(self.url)
                    could_not_load = html.msg != 'OK'
                except urllib2.HTTPError as e:
                    print e
                    could_not_load = True

                if could_not_load:
                    raise MultipleSourceLoadError("Could not load source properly from " + self.url)

                # Parsing document
                try:
                    r = html5parser.parse(html)
                except etree.ParseError:
                    dump = open('msabstract_parsing_error_dump.txt', 'w')
                    dump.write(html.read())
                    raise MultipleSourceLoadError("Could not parse this document, dumped in " + dump.name)

                if r:
                    abstract_parts = r.xpath(info["xpath"],
                                             namespaces={'x': 'http://www.w3.org/1999/xhtml'})
                    if abstract_parts is not None:
                        self.objects[source_id] = '\n'.join(
                                [p.text_content() for p in abstract_parts]
                        )
                        if dump_parts:
                            f = open('msabstract_dump_results.html', 'w')
                            f.write(etree.tostring(r, pretty_print=True))
                    else:
                        dump = open('msabstract_no_abstract.txt', 'w')
                        dump.write(etree.tostring(r, pretty_print=True))
                        raise MultipleSourceLoadError("Error while reading parsed answer.")
                    break

    def __str__(self):
        return '\n'.join([k + " : " + v for k, v in self.objects.iteritems()])


class MultipleSourceLoadError(Exception):
    def __init__(self, message):
        self.message = "MultipleSource : error while loading source :\n" + message + "\n"
        super(MultipleSourceLoadError, self).__init__()

    def __str__(self):
        return self.message


class MSTests(BaseTest):
    TEST_NAME = "Multiple source tests"

    def run(self):
        t1 = self.test_abstract_acm()
        self.passed = t1
        self.runs += 1

    @staticmethod
    def test_abstract_acm():
        a = MSAbstract("http://dl.acm.org/citation.cfm?doid=2723372.2749433")
        a.load(dump_parts=True)
        return len(a.objects) > 0
