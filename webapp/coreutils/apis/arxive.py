# coding=UTF-8

import feedparser
from datetime import datetime

from requests.exceptions import HTTPError

from coreutils.tests import BaseTest

ALL_STAT_CATEGORIES = "stat.AP+stat.CO+stat.ML+stat.ME+stat.TH"

ALL_QBIO_CATEGORIES = "q-bio.BM+q-bio.CB+q-bio.GNK+q-bio.MN+q-bio.NC+q-bio.OT+q-bio.PE+" \
                      "q-bio.QM+q-bio.SC+q-bio.TO"

ALL_CS_CATEGORIES = "cs.AR+cs.CL+cs.CC+cs.CE+cs.CG+cs.GT+cs.CV+cs.CY+cs.CR+cs.DS+cs.DB+" \
                    "cs.DL+cs.DM+cs.DC+cs.GL+cs.GR+cs.HC+cs.IR+cs.IT+cs.LG+cs.LO+cs.MS+" \
                    "cs.MA+cs.MM+cs.NI+cs.NE+cs.NA+cs.OS+cs.OH+cs.PF+cs.PL+cs.RO+cs.SE+" \
                    "cs.SD+cs.SC"

ALL_NLIN_CATEGORIES = "nlin.AO+nlin.CG+nlin.CD+nlin.SI+nlin.PS"

ALL_MATH_CATEGORIES = "math.AGmath.AT+math.AP+math.CT+math.CA+math.CO+math.AC+math.CV+" \
                      "math.DG+math.DS+math.FA+math.GM+math.GN+math.GT+math.GR+math.HO+" \
                      "math.IT+math.KT+math.LO+math.MP+math.MG+math.NT+math.NA+math.OA+" \
                      "math.OC+math.PR+math.QA+math.RT+math.RA+math.SP+math.ST+math.SG"

ARXIV_DATEFORMAT = "%Y-%m-%dT%H:%M:%SZ"


class ArxivQuery(object):
    QUERY_URL = "http://export.arxiv.org/api/query?" \
                "search_query=%s&" \
                "id_list=%s&" \
                "start=%d&" \
                "max_results=%d"
    RESULTS_PER_PAGE = 20

    def __init__(self,
                 s=None,
                 title=None,
                 author=None,
                 abstract=None,
                 comment=None,
                 journal_reference=None,
                 report_number=None,
                 categories=None,
                 id_list=None
                 ):
        """
        Each parameter can have a boolean operator prefix : _AND_ or _OR_. For example if you want to search
        an article on parallel computing whose author is Jane Doe :
        ArxivQuery("parallel computing", author="_AND_Jane Doe")

        :param s: string to search for in any category
        :param title: title of the publication
        :param author: author of the publication. If several authors, separate them by +
        :param abstract: abstract of the publication
        :param comment: comment in arxiv
        :param journal_reference: search for a journal reference
        :param report_number: search for a report number
        :param categories: arxiv categories, see ALL_CS_CATEGORIES for Computer Science, or on
            http://arxiv.org/help/api/user-manual#subject_classifications
        :param id_list: arxiv ids of the publications
        """
        self.search_parts = {
            "all": s,
            "ti": title,
            "au": author,
            "abs": abstract,
            "co": comment,
            "rn": report_number,
            "cat": categories,
            "jr": journal_reference
        }

        self.last_url = ""
        self.page = 0
        self.max_pages = -1
        self.results = -1
        self.search_query = self._search_query_string_builder(self.search_parts)
        self.id_list = ""
        if id_list is not None:
            self.id_list = ",".join(str(id_list))

    @staticmethod
    def _search_query_string_builder(parts):
        """
        See http://arxiv.org/help/api/user-manual#query_details for details concerning
        Arxive API.
        Does not fully support boolean operators, only AND, OR btw different types
        :param parts: the parts of the search query string, as specified in the manual
        :return: the search_string parameter of the HTTP query
        """
        search_query_parts = []
        for k, v in parts.iteritems():
            if v is not None:
                if isinstance(v, unicode):
                    sval = v.encode(encoding="utf-8")
                elif isinstance(v, str):
                    sval = v
                else:
                    raise Exception("Expected a string for %s" % k)
                boolean = ""
                if sval.startswith("_AND_"):
                    boolean = "AND+"
                    sval = sval[5:]
                if sval.startswith("_OR_"):
                    boolean = "OR+"
                    sval = sval[4:]
                sqpart = sval
                if sval.startswith("%22") and sval.endswith("%22") and sval.count("%22") == 2:
                    sqpart = sval[3:len(sval) - 3]
                search_query_parts.append(boolean + k + ":%22" + sqpart.replace(' ', '+') + "%22")
        return "+".join(search_query_parts)

    def _query_current_page(self):
        self.last_url = self.QUERY_URL % (self.search_query,
                                          self.id_list,
                                          self.page * self.RESULTS_PER_PAGE,
                                          self.RESULTS_PER_PAGE)
        results = feedparser.parse(self.last_url)
        self.results = int(results.feed.opensearch_totalresults)
        self.max_pages = self.results / self.RESULTS_PER_PAGE
        if results['status'] != 200:
            raise HTTPError("HTTP Error " + str(results['status']) + " in query")
        else:
            results = results["entries"]
            for result in results:
                mod_query_result(result)

        return results

    def get_current_page(self):
        return self._query_current_page()

    def has_next_page(self):
        return not self.page == self.max_pages

    def next_page(self):
        if self.has_next_page():
            self.page = min(self.page + 1, self.max_pages)
            return self._query_current_page()
        else:
            return None

    def previous_page(self):
        self.page = max(self.page - 1, 0)
        return self._query_current_page()

    def __str__(self):
        if self.results == -1:
            return "Axrive query not executed yet."
        s = "Arxive query, last url queried : %s\n-> %d results.\n" % (self.last_url.replace("%22", "|"),
                                                                       self.results)
        s += "Page %d / %d." % (self.page, self.max_pages)

        return s

    # Static utilities
    @staticmethod
    def is_cs_result(result):
        return result["supercategory"] == "cs"


# Pruning and normalizing the results
def mod_query_result(result):
    # Useful to have for download automation
    result['pdf_url'] = None
    for link in result['links']:
        if 'title' in link and link['title'] == 'pdf':
            result['pdf_url'] = link['href']

    result['affiliation'] = result.pop('arxiv_affiliation', 'None')
    result['arxiv_url'] = result.pop('link')
    result['title'] = result['title'].rstrip('\n')
    result['summary'] = result['summary'].rstrip('\n')
    result['authors'] = [d['name'] for d in result['authors']]
    result['supercategory'] = result['arxiv_primary_category']['term'].split('.')[0]

    if 'published' in result:
        date_strring = result['published']
        result['published'] = datetime.strptime(date_strring, ARXIV_DATEFORMAT)

    if 'updated' in result:
        date_strring = result['updated']
        result['updated'] = datetime.strptime(date_strring, ARXIV_DATEFORMAT)

    if 'arxiv_comment' in result:
        result['arxiv_comment'] = result['arxiv_comment'].rstrip('\n')
    else:
        result['arxiv_comment'] = None
    if 'arxiv_journal_ref' in result:
        result['journal_reference'] = result.pop('arxiv_journal_ref')
    else:
        result['journal_reference'] = None
    if 'arxiv_doi' in result:
        result['doi'] = result.pop('arxiv_doi')
    else:
        result['doi'] = None


# Tests class
class ArxiveTests(BaseTest):
    TEST_NAME = "Arxive Tests"

    def run(self):
        self.passed = self.test1()
        self.runs += 1

    def test1(self):
        arxive_query = ArxivQuery(author="Michel Habib", title="_AND_A Note On Computing Set Overlap Classes")
        try:
            results = arxive_query.get_current_page()
            self.print_verb(arxive_query.last_url)
        except HTTPError:
            return False
        self.print_verb("Got %d results." % len(results))
        for result in results:
            self.print_verb(str(result.title))
        self.print_verb(str(arxive_query))
        arxive_query.next_page()
        self.print_verb(str(arxive_query))
        return True
