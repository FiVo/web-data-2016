# coding=UTF-8

import pandas as pd

from coreutils.apis.imported.pygoogle import pygoogle
from coreutils.tests import BaseTest


class CSResearcherSearch(object):
    def __init__(self, researcher_name, additional_info=None):
        self._google_results = None
        self.filtered_results = None
        query_string = ""
        if additional_info is not None:
            query_string = " ".join(additional_info)

        query_string = " ".join([researcher_name, query_string])

        self.query = pygoogle(query_string)
        self.loaded = False

    def search(self):
        self._google_results = self.query.search()
        return self

    def _print_raw(self):
        if self._google_results is not None:
            for data in self._google_results:
                print data

    def print_results(self):
        if self._google_results is not None:
            for title, result in self._google_results.iteritems():
                print "[%s" + title
                print "\tURL : %s" % result["url"]
                print "\tContent : " + result["content"]
        else:
            print "Query not executed."

    def filter_results(self):
        store = []
        for title, result in self._google_results.iteritems():
            store.append([title, result["url"], result["content"]])
        df = pd.DataFrame(data=store, columns=("Title", "Url", "Content"))
        print df


class GoogleSearchTest(BaseTest):
    TEST_NAME = "Google search test"

    def run(self):
        self.passed = self.base_test()
        self.runs += 1

    @staticmethod
    def base_test():
        csr = CSResearcherSearch("Michel Habib")
        csr.search()
        csr.filter_results()
        return True
