import urllib
import urllib2
from BaseHTTPServer import BaseHTTPRequestHandler
from lxml import etree

# in case of problems check out
# http://dev.elsevier.com/tecdoc_api_authentication.html
# for more information
# Implement validity of AUTHTOKEN checking

# implement author search
# implement abstract retrieval
# implement article retrieval

# API_KEY = "3057297a4ceed314a327b2cfa00336d5"
API_KEY = "ef2ed512e634d30fa54b06f2862b8775"
# BASE_URL = "http://api.elsevier.com/content/search/author?"
BASE_URL = "http://api.elsevier.com/authenticate?platform=SCOPUS"

values = {'apiKey': API_KEY
          # 'query' : 'authfirst(Pierre)%20AND%20authlast(Senellart)'
          # 'co-author' : 10
          }

# scopus_author_search_url = 'http://api.elsevier.com/content/search/author?'
request_headers = {"Accept": "text/xml", "X-ELS-APIKey": API_KEY}
# search_query = 'query=AUTHFIRST(%) AND AUTHLASTNAME(%)' % ('Michel', 'COMP')
# data = urllib.urlencode(values)

req = urllib2.Request(BASE_URL, headers=request_headers)
# print req.get_full_url()
try:
    response = urllib2.urlopen(req)
    the_page = response.read()
    print the_page
except urllib2.HTTPError as e:
    BaseHTTPRequestHandler.responses[e.getcode()]

try:
    # check if this can be done more efficiently
    doc = etree.fromstring(the_page)
    authtoken = doc.xpath("./authtoken/descendant::text()")
    print authtoken[0]
    # doc = etree.parse(urllib2.urlopen(req.encode('utf-8')))
except ImportError:
    print "Error parsing ! "

scopus_author_search_url = 'http://api.elsevier.com/content/search/author?'
request_headers = {"Accept": "application/xml", "X-ELS-APIKey": API_KEY}  # , "X-ELS-Authtoken" : authtoken[0]}
values = {'query': 'AUTHFIRST(%s) AND AUTHLASTNAME(%s)' % ('Michel', 'Habib')}
data = urllib.urlencode(values)
encoded_header = urllib.urlencode(request_headers)

# encoded_url = scopus_author_search_url + encoded_header + data

# print encoded_url

# page_request = requests.get(scopus_author_search_url + data, headers = request_headers)
req = urllib2.Request(scopus_author_search_url + data, headers=request_headers)

print "\n\n"
# print page_request.content.
# doc = etree.parse(page_request.text)
# print etree.tostring(doc,pretty_print = True)
# req = urllib2.Request(scopus_author_search_url, data, headers = request_headers)
# print req.get_full_url()
try:
    response = urllib2.urlopen(req)
    the_page = response.read()
    doc = etree.fromstring(the_page)
    print etree.tostring(doc, pretty_print=True)
except urllib2.HTTPError as e:
    print BaseHTTPRequestHandler.responses[e.getcode()]
