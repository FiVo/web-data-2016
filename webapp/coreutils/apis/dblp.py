# coding=UTF8
import urllib2
from BaseHTTPServer import BaseHTTPRequestHandler
from collections import namedtuple
from lxml import etree

import microdata

from ..tests import BaseTest

DBLP_BASE_URL = 'http://dblp.uni-trier.de/'


def first_or_none(seq):
    try:
        return next(iter(seq))
    except StopIteration:
        pass


# base class for dblp queries. A little bit void yet ...
class DblpQuery(object):
    BASE_URL = DBLP_BASE_URL
    RESULT_BASE_URL = ''
    GOAL_TYPE = "[http://schema.org/Thing]"

    def __init__(self, lazy_attrs):
        self.lazy_attrs = None
        if lazy_attrs is not None:
            self.lazy_attrs = set(lazy_attrs)
        self.data = None

    def load_data(self):
        pass


# Researcher queries allows us to get researcher names, and coauthors
class ResearcherQuery(DblpQuery):
    BASE_URL = DBLP_BASE_URL + 'search/author?xauthor='
    COAUTHORS_BASE_URL = DBLP_BASE_URL + 'pers/xc/'
    DBLP_KEY_BASE_URL = DBLP_BASE_URL + 'pers/xk/'
    AUTHOR_BASE_URL = DBLP_BASE_URL + 'pers/hd/'
    GOAL_TYPE = microdata.URI("http://schema.org/Person")

    def __init__(self, researcher_name, urlpt=None):
        super(ResearcherQuery, self).__init__(None)

        if isinstance(researcher_name, unicode):
            self.query_suffix = researcher_name
        else:
            self.query_suffix = unicode(researcher_name, 'utf-8')

        self.query_url = self.BASE_URL + self.query_suffix.replace(" ", ":")
        # urlpt is used as an identifier, the object can be initialized using the results of get_url()
        if urlpt is not None and urlpt.startswith(self.DBLP_KEY_BASE_URL):
            urlpt = urlpt[len(self.DBLP_KEY_BASE_URL):]
        self.urlpt = urlpt
        self._suggestions_ = []
        self._is_matching_query = urlpt is not None
        # if the urlpt has not been provided, we have to search for the author
        if urlpt is None:
            try:
                self._is_matching_query = self._matches()
            except NoMatchException as ne:
                print ne.__unicode__()
                self._is_matching_query = False

    # private method called upon init to see if query matches
    def _matches(self):
        try:
            doc = etree.parse(urllib2.urlopen(self.query_url.encode('utf-8')))
        except ImportError:
            print "Error parsing file"
            return False

        authors = doc.xpath("./author")
        # The authors we get are prefix-matching author names,
        # but here we want to restrict search and have only
        # exactly matching names
        for author in authors:
            self._suggestions_.append(author.text)
            if author.text.lower() == self.query_suffix.lower():
                self.urlpt = author.get("urlpt")
                # Load personal data
                url = self.AUTHOR_BASE_URL + self.urlpt
                items = microdata.get_items(urllib2.urlopen(url))
                # Check it's a Person, and the right one
                for item in items:
                    if self.GOAL_TYPE in item.itemtype and item.name is not None \
                            and item.name.lower() == unicode(self.query_suffix).lower():
                        self.data = item
                        break
                return True

        raise NoMatchException(doc.xpath("./author/descendant::text()"))

    # Public interface
    def matches(self):
        return self._is_matching_query

    def check_access(self):
        # We can always access Dblp, wherever we are !
        return True

    def get_researcher_name(self):
        return self.query_suffix

    # Coauthors
    def get_coauthors_url(self):
        if self._is_matching_query:
            return self.COAUTHORS_BASE_URL + self.urlpt
        else:
            raise StandardError

    def get_coauthors(self):
        coauthors = []
        if self.matches():
            while True:
                try:
                    doc = etree.parse(urllib2.urlopen(self.get_coauthors_url()))
                    break
                except ImportError:
                    print "Error parsing file"
                    return None
                except urllib2.HTTPError as e:
                    if e.getcode() == 429:
                        raise e
                    else:
                        raise Exception("URLError in get_coauthors : " + self.get_coauthors_url())

            coauthors_xml = doc.xpath("./author")
            for coauth in coauthors_xml:
                coauthors.append(
                        ResearcherQuery(coauth.xpath("./descendant::text()")[0], urlpt=coauth.get("urlpt"))
                )
            return coauthors
        return None

    def get_keys_url(self):
        if self._is_matching_query:
            return self.DBLP_KEY_BASE_URL + self.urlpt

    def get_keys(self):
        keys = []
        try:
            doc = etree.parse(urllib2.urlopen(self.get_keys_url()))
        except ImportError:
            print "Error parsing key file"
            return None
        keys.extend(doc.xpath("./dblpkey[not(@type)]/descendant::text()"))
        return keys

    def get_suggestions(self):
        return self._suggestions_

    def get_url(self):
        return self.get_keys_url()


class RecordQuery(DblpQuery):
    # All records are stored in a local xml file
    # RECORDS_FILE = 'dblp.xml'
    Publisher = namedtuple('Publisher', ['name', 'href'])
    Series = namedtuple('Series', ['text', 'href'])
    Citation = namedtuple('Citation', ['reference', 'label'])

    # url for retrieving
    RECORDS_URL = 'http://dblp.uni-trier.de/rec/bibtex/'

    # Don't start looking in the xml file at object initialization
    def __init__(self, key):
        self.key = key
        self.query_url = self.RECORDS_URL + key + '.xml'
        self.loaded = False
        super(RecordQuery, self).__init__(
                ['type', 'sub_type', 'mdate',
                 'authors', 'editors', 'title', 'year', 'month', 'journal',
                 'volume', 'number', 'chapter', 'pages', 'ee', 'isbn', 'url',
                 'booktitle', 'crossref', 'publisher', 'school', 'citations',
                 'series']
        )

    def _get_record_url(self):
        return self.query_url

    def load_data(self):
        return self._load(force_update=False)

    def _load(self, force_update):
        # check if the information about the record has already been loaded
        # and we do not want to force the update
        if self.loaded and not force_update:
            return True

        else:
            # doc = etree.parse(self.RECORDS_FILE)
            # record_path = "./*[@key=\'" + self.key + "\']"
            try:
                record = etree.parse(urllib2.urlopen(self._get_record_url()))
            except urllib2.HTTPError as e:
                # TODO got too many requests here
                print BaseHTTPRequestHandler.responses[e.getcode()]
                if e.getcode() == 429:
                    print "Too many requests"
                return

            publication = first_or_none(record.xpath('/dblp/*[1]'))
            if publication is None:
                return False

            self.data = {
                'type': publication.tag,
                'sub_type': publication.attrib.get('publtype', None),
                'mdate': publication.attrib.get('mdate', None),
                'authors': publication.xpath('author/text()'),
                'editors': publication.xpath('editor/text()'),
                'title': first_or_none(publication.xpath('title/text()')),
                'year': int(first_or_none(publication.xpath('year/text()'))),
                'month': first_or_none(publication.xpath('month/text()')),
                'journal': first_or_none(publication.xpath('journal/text()')),
                'volume': first_or_none(publication.xpath('volume/text()')),
                'number': first_or_none(publication.xpath('number/text()')),
                'chapter': first_or_none(publication.xpath('chapter/text()')),
                'pages': first_or_none(publication.xpath('pages/text()')),
                'ee': first_or_none(publication.xpath('ee/text()')),
                'isbn': first_or_none(publication.xpath('isbn/text()')),
                'url': first_or_none(publication.xpath('url/text()')),
                'booktitle': first_or_none(publication.xpath('booktitle/text()')),
                'crossref': first_or_none(publication.xpath('crossref/text()')),
                'publisher': first_or_none(publication.xpath('publisher/text()')),
                'school': first_or_none(publication.xpath('school/text()')),
                'citations': [self.Citation(c.text, c.attrib.get('label', None))
                              for c in publication.xpath('cite') if c.text != '...'],
                'series': first_or_none(self.Series(s.text, s.attrib.get('href', None))
                                        for s in publication.xpath('series'))
            }

            self.loaded = True
            return True

    def get_title(self):
        return self.record.xpath("//title/descendant::text()")

    def get_authors(self):
        pass

    def __eq__(self, other):
        return self.key == other.key

    def __ne__(self, other):
        return self.key != other.key


# Error classes
class NoMatchException(Exception):
    def __init__(self, suggestions):
        self.suggestions = suggestions

    def __unicode__(self):
        if self.suggestions.__len__() > 0:
            s = u"Query didn't match, but here are some similar names you can try : \n" + \
                unicode.join(u'\n', self.suggestions)
            return s
        else:
            return u"Query didn't match, and there is no suggestions !\n"


# Some tests
class DblpTests(BaseTest):
    TEST_NAME = "Dblp tests"

    def run(self):
        # test1 = not self.test_query_not_matching_with_suggestions()
        # test2 = not self.test_query_not_matching_strict()
        # test3 = self.test_queries_matching() and self.test_queries_unicode()
        # test5 = self.test_functionalities()
        # test4 = self.test_record_query()
        test6 = self.test_personal_data()
        self.passed = test6
        self.runs += 1

    @staticmethod
    def test_query_not_matching_with_suggestions():
        print "Test appproximate query"
        return ResearcherQuery("sergey n.").matches()

    @staticmethod
    def test_query_not_matching_strict():
        print "Test strictly noy matching query"
        return ResearcherQuery("vofhfsdqiufn").matches()

    @staticmethod
    def test_queries_matching():
        print "Test matching queries..."
        # Should return True
        researchers = ["Serge abiteboul", "serge abiteboul",
                       "serge Abiteboul", "pierre senellart"]
        for researcher in researchers:
            if not ResearcherQuery(researcher).matches():
                print "Did not match for " + researcher
                return False
        return True

    def test_queries_unicode(self):
        # Should return True
        print "Test queries with some weird chars"
        researchers = [u"Øyvind Aardal", u"Michel Böhms"]
        for researcher in researchers:
            if not ResearcherQuery(researcher).matches():
                self.print_verb(" ".join(["Did not match for ", researcher]))
                return False
        return True

    def test_functionalities(self):
        researchers = [u'Albert Cohen', u'Marc Pouzet', u'Timothy Bourke']
        for researcher in researchers:
            rq = ResearcherQuery(researcher)
            if not rq.matches():
                return False
            else:
                s = "Query matches for " + rq.query_suffix
                self.print_verb(s)

            if rq.get_coauthors() is None:
                return False
            else:
                msg = "Here are his coauthors :"
                msg += "\n".join([coauth.query_suffix for coauth in rq.get_coauthors()])
                self.print_verb(msg)

            if rq.get_keys() is None:
                return False
            else:
                self.print_verb("Keys ok !")

        return True

    def test_record_query(self):
        rquery = RecordQuery("journals/acta/Saxena96")
        rquery.RECORDS_FILE = 'text-dblp.xml'
        rquery.load_data()
        for k, v in rquery.data.iteritems():
            if v is not None:
                s = k + " : " + str(v)
                self.print_verb([s])
        return rquery.loaded

    def test_personal_data(self):
        researchers = ["Pierre Senellart", "Michel Habib", "Albert Cohen"]
        for researcher in researchers:
            rq = ResearcherQuery(researcher)
            if rq.data is not None:
                self.print_verb(*[researcher, rq.data])
            else:
                err_msg = "No data extracted for " + researcher
                self.print_verb(err_msg)
                return False
        return True
