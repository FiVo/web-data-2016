# !/usr/bin/python
# coding=UTF8


# Interface for queries concerning authors
# Any specific information retrieval should pass through this interface
# so application and retrieval are well separated

import Queue
import json
import urllib2
from json.decoder import WHITESPACE

import microdata

from apis.arxive import ArxivQuery
from apis.customSearch import ResearcherCustomSearch
from apis.dblp import ResearcherQuery, RecordQuery
from apis.elsevier import ResearcherQuery as RQ_elsevier
from papers import Publication, PublicationException, PublicationEncoder, PublicationDecoder
from tests import BaseTest
from utils import Error, BadAPI, flatten_and_remove_duplicates

DEFAULT_PUBLICATIONS_PER_AUTHOR = 10


# Assumes researcher has been called
# computes the collaboration distance
# essentially just a 2-sided BFS
def distance(name1, name2):
    if name1 == name2:
        return 0
    q = Queue.Queue()
    q.put(name1)
    q.put(name2)
    name_dis = dict([(name1, (0, True)), (name2, (0, False))])
    while not q.empty():
        cur = q.get()
        cur_co = ResearcherQuery(cur).get_coauthors()

        for co_au in cur_co:
            if co_au in name_dis and name_dis[co_au][1] != name_dis[cur][1]:
                return name_dis[co_au][0] + 1 + name_dis[cur][0]
            elif not (co_au in name_dis):
                name_dis[co_au] = (name_dis[cur][0] + 1, name_dis[cur][1])
                q.put(co_au)


class Person(object):
    TYPE = microdata.URI("http://schema.org/Person")

    def __init__(self):
        # Attributes are Person's attributes from Schema.Org look at
        # http://schema.org/Person for more information
        self.attrs = {
            "name": None,  # "Complete name", 18],
            "firstName": None,  # "First name", 0],
            "lastName": None,  # "Last name", 1],
            "additionalName": None,  # "Additional name information", 2],
            "address": None,  # "Address", 3],
            "affiliation": None,  # "Affiliation to an organization", 4],
            "alumniOf": None,  # "Alumni of an organization or educational organization", 5],
            "award": None,  # "Received an award", 6],
            "birth_info": None,  # "Birth information", 7],
            "death_info": None,  # "Death information", 8],
            "email": None,  # "Email", 9],
            "honoricPrefix": None,  # "Honorific prefix", 10],
            "honoricSuffix": None,  # "Honorific suffix", 11],
            "jobTItle": None,  # "Job title", 12],
            "memberOf": None,  # "Member of an organization", 13],
            "nationality": None,  # "Nationality", 14],
            "workLocation": None,  # "Work location", 15],
            "additionalType": None,  # "URL or identifier", 16],
            "sameAs": None  # "URL identifying unambiguously the object", 17]
        }


class Researcher(Person):
    """
    This class represents a Researcher.
    """
    APIS = {
        ResearcherQuery: "dblp",
        RQ_elsevier: "elsevier"
    }

    def __init__(self, name, *queries):
        """
        :param name: the name of the researcher
        :param queries: either a single query object or a list of query objects
        """
        self.name = ' '.join(name.split(' '))
        self.suggestions = []
        self.papers_queried = False
        self.papers = None
        self.coauthors = []
        self._queries = {}
        self.url = ""
        # if no query has been provided, we use Dblp
        if queries is None or len(queries) == 0:
            query = ResearcherQuery(self.name)
            queries = [query]
            self._queries = {"dblp": query}
            self.url = query.get_keys_url()
        super(Researcher, self).__init__()

        for rq in queries:
            if rq.__class__ not in self.APIS:
                raise BadAPI("query type not recognized.")

            if rq.matches():
                if isinstance(rq, ResearcherQuery):
                    self.url = rq.get_keys_url()
                self._queries[self.APIS.get(rq.__class__)] = rq
            else:
                # TODO : now don't the query if it doesn't match,
                # we should change it for the "best match"
                self.suggestions = rq.get_suggestions()

            try:
                if rq.data is not None:
                    for key in rq.data.props:
                        self.attrs[key] = rq.data.props[key]
            except AttributeError:
                # TODO : happens when researcher info is extracted from elsevier, solve this
                pass

    def _load_papers(self, nb_publication):
        # TODO DOI is the full link in DBLP and only the extension in elsevier
        # First get a batch of papers from Arxiv, the fastest APi we have now
        try:
            papers_map = dict(
                    [(pub.get_slugified_title(), pub)
                     for pub in
                     Publication.from_arxiv(ArxivQuery(author=self.get_name_str()),
                                            max_pubs=DEFAULT_PUBLICATIONS_PER_AUTHOR)
                     ]
            )

        except PublicationException:
            papers_map = dict()

        # Then query other APIs for more
        for api_name, query in self._queries.iteritems():
            if isinstance(query, ResearcherQuery):
                keys = query.get_keys()
                for i in range(min(nb_publication, len(keys))):
                    try:
                        pub = Publication(RecordQuery(keys[i]))
                    except urllib2.HTTPError as e:
                        pass
                    slug = pub.get_slugified_title()
                    if slug in papers_map:
                        print pub.get_title()
                        pub0 = papers_map.get(slug)
                        merged = pub.merge(pub0)
                        if merged:
                            print pub.exists
                            print "DBLP : Just merged on %s" % pub.get_title()
                    papers_map.update({slug: pub})

                self.papers_queried = True

            elif isinstance(query, RQ_elsevier):
                for abst in query.abstracts(nb_publication):
                    pub = Publication(abstract=abst)
                    slug = pub.get_slugified_title()
                    if slug in papers_map:
                        pub0 = papers_map.get(slug)
                        merged = pub.merge(pub0)
                        if merged:
                            print "ELSEVIER : Just merged on %s" % slug
                    papers_map.update({slug: pub})

                self.papers_queried = True
        self.papers = list(papers_map.values())

    def add_source(self, source):
        if source.__class__ in self.APIS:
            self._queries[self.APIS.get(source.__class__)] = source
        else:
            raise BadAPI("query type not recognized.")

    def get_coauthors_from(self, options=("dblp",)):
        query_instances = {
            "dblp": ResearcherQuery,
            "elsevier": RQ_elsevier
        }
        for option in options:
            if option not in self._queries:
                raise Exception("This api is not implemented for Researchers : %s" % option)
            query = self._queries[option]
            if isinstance(query, query_instances[option]) and query.check_access():
                self.coauthors = [
                    Researcher(rq.get_researcher_name(), rq) for rq in self._queries[option].get_coauthors()
                    ]

            else:
                raise Error("Not correct object"
                            "Cannot get author from Dblp if he"
                            "doesnt exist on Dblp")
        return self.coauthors

    def get_defined_attributes(self):
        attributes = {"name": self.name}
        for k, v in self.attrs.iteritems():
            if v is not None:
                values = []
                for value in v:
                    if isinstance(value, microdata.Item):
                        values.append(json.loads(value.json()))
                    elif isinstance(value, microdata.URI):
                        values.append(str(value))
                    else:
                        values.append(v)
                attributes[k] = values
        # Flatten elements in attributes and remove duplicates
        return flatten_and_remove_duplicates({"type": ["http://www.schema.org/Person"],
                                              "properties": attributes})

    def get_distance_to(self, author2, recursion_depth_limit=20):
        if self.name == author2.name:
            return 0

        q = Queue.Queue()
        name_dis = {}
        q.put((self, 0))
        q.put((author2, 0))

        name_dis[self.name] = (0, True)
        name_dis[author2.name] = (0, False)

        while not q.empty():
            cur, cur_depth = q.get()
            cur_co = cur.get_coauthors_from()

            if cur_depth >= recursion_depth_limit:
                return -1

            for co_au in cur_co:
                if co_au.name in name_dis and name_dis[co_au.name][1] != name_dis[cur.name][1]:
                    return name_dis[co_au.name][0] + 1 + name_dis[cur.name][0]
                elif not (co_au.name in name_dis):
                    name_dis[co_au.name] = (name_dis[cur.name][0] + 1, name_dis[cur.name][1])
                    q.put((co_au, cur_depth + 1))

    # It's just returning self.name, but we might add support for firstname + lastname
    def get_name_str(self):
        if isinstance(self.name, unicode):
            return str(self.name.encode('utf-8'))
        return str(self.name)

    def get_papers(self, nb_publication=DEFAULT_PUBLICATIONS_PER_AUTHOR, force_query=False):
        if self.papers_queried and not force_query:
            return self.papers
        else:
            self._load_papers(nb_publication)
            return self.papers

    def get_suggestions(self):
        return self.suggestions

    def get_unique_identifier(self):
        if "dblp" in self._queries:
            return self._queries["dblp"].get_keys_url()
        else:
            return self.name

    def not_found(self):
        if self._queries is None or len(self._queries) == 0:
            return True
        else:
            for _, query in self._queries.iteritems():
                if not query.matches():
                    self.suggestions.extend(query.get_suggestions())
                    return True

    def more(self):
        search_query = self.name
        rcs = ResearcherCustomSearch(search_query)
        rcs.load()


class ResearcherEncoder(json.JSONEncoder):
    def encode(self, o):
        assert isinstance(o, Researcher)
        pub_enc = PublicationEncoder()
        json_object = {
            "name": o.name,
            "url": str(o.url),
            "suggestions": o.suggestions,
            "attrs": o.get_defined_attributes()
        }
        if o.coauthors is not None:
            json_object["coauthors"] = [self.encode(coauthor) for coauthor in o.coauthors]
        if o.papers is not None:
            json_object["papers"] = [pub_enc.encode(pub) for pub in o.papers]

        json.dumps(json_object)


class ResearcherDecoder(json.JSONDecoder):
    def decode(self, s, _w=WHITESPACE.match):
        pub_dec = PublicationDecoder()
        json_object = json.loads(s)
        researcher = Researcher(
                json_object["name"],
                ResearcherQuery(json_object["name"], json_object["url"])
        )
        researcher.papers = []
        if "papers" in json_object:
            for paper_repr in json_object["paper"]:
                researcher.papers.append(pub_dec.decode(paper_repr))
            researcher.papers_queried = True

        researcher.coauthors = []
        if "coauthors" in json_object:
            for coauthor in json_object["coauthors"]:
                researcher.coauthors.append(self.decode(coauthor))

        researcher.attrs = json_object["attrs"]

        return researcher


class AuthorTests(BaseTest):
    TEST_NAME = "Author file tests."

    def run(self):
        # t1 = self.run_test1()
        t2 = self.run_test_distance()
        # t3 = self.run_elsevier_test()
        # t4 = self.run_test_urlpt_is_id()
        # t5 = self.test_attributes()
        # t6 = self.test_info_of_papers()
        self.passed = t2
        self.runs += 1

    def run_test1(self):
        albert = Researcher(u'Michel Habib')
        # oyvind is a real researcher, he should exist
        passed = not albert.not_found()
        if passed:
            self.print_verb(albert.name + u" is a real researcher !")

        # Did oyvind write papers ?
        if len(albert.get_papers()) == 0:
            self.print_verb(u"No papers for " + albert.name)
            passed = False
        else:
            self.print_verb(u"We have papers from " + albert.name)
        coauthors = albert.get_coauthors_from()

        if len(coauthors) == 0:
            passed = False
        else:
            self.print_verb(albert.name + u" has coauthors !")
        return passed

    def run_test_distance(self):
        passed = False
        # albert = Researcher(u'Michel Habib')
        # marc = Researcher(u'Pierre Senellart')
        albert = Researcher(u'Balthazar Bauer')
        marc = Researcher(u'Alan M. Turing')
        dist = albert.get_distance_to(marc)
        if dist > 1:
            passed = True
        print u"Distance between " + albert.name + u" and " + marc.name + u" : " + str(dist)
        return passed

    def run_elsevier_test(self):
        print "Testing elsevier queries"
        michel = Researcher("Michel Habib", RQ_elsevier("Michel Habib"))
        for paper in michel.get_papers():
            print paper.get_title() + "\n"
            print paper.get_abstract() + "\n"

    def run_test_additional_attributes(self):
        researchers = [
            Researcher(u"Pierre Senellart"),
            Researcher("MICHEL HABIB"),
            Researcher("Marc Pouzet"),
            Researcher("Pierre Fraigniaud"),
            Researcher("Mary Sheeran")
        ]
        for researcher in researchers:
            name = researcher.attrs.get("name")
            if name is None:
                return False
            else:
                self.print_verb("Name found in microdata : " + str(name))

            try:
                same_as = researcher.attrs.get("sameAs")
                self.print_verb("SameAs attribute is : " + str(same_as))
            except AttributeError:
                self.print_verb("Not SameAs attribute")

        return True

    def run_test_urlpt_is_id(self):
        mary = Researcher("Mary Sheeran")
        url = mary.url
        mary2 = Researcher("Mary Sheeran", ResearcherQuery(mary.name, urlpt=url))
        self.print_verb("Mary : %s \nMary 2 %s" % (mary.url, mary2.url))
        return mary2.url == mary.url

    def test_attributes(self):
        d = Researcher("Pierre Senellart").get_defined_attributes()
        if len(d) > 0:
            self.print_verb(str(d))
            return True
        else:
            return False

    def test_info_of_papers(self):
        import pprint
        monica = Researcher("Tova Milo")
        papers = monica.get_papers()
        print len(papers)
        # Dump the papers
        fd = open("dump_paper_info.txt", "w")
        fd.write(monica.name.encode('utf8'))
        fd.write("\n")
        for paper in papers:
            fd.write(pprint.pformat(paper.get_schema()))
            fd.write(str("\n-------\n"))
        fd.close()
        return True
