import getopt
import sys

from authors import Researcher


def main(res1, res2=None):
    r1 = Researcher(res1)
    if res2 is None:
        for paper in r1.get_papers():
            print paper.get_title()
    else:
        if r1.not_found():
            print "Couldn't find " + res1 + " in any Database"
            return
        r2 = Researcher(res2)
        if r2.not_found():
            print "Couldn't find " + res2 + " in any Database"
            return
        print "Distance between " + res1 + " and " + res2 + \
              " is " + str(r1.get_distance_to(r2))
        return


# Parse command line arguments

# Available options :
options = "h"
long_options = ["papers", "dist", "a1=", "a2="]


def print_options():
    print "The followings options are :"
    print "--papers --a1=[author name] \t\t\t get the papers of one author"
    print "--dist --a1=[auth. name] --a2=[auth.name]\t get the " \
          "collaboration distance between two authors"


def parse_options(argv):
    try:
        opts, args = getopt.getopt(argv, options, long_options)
    except getopt.GetoptError:
        print_options()
        sys.exit(2)
    if len(opts) == 0:
        print_options()

    get_distance = False
    get_papers = False
    a1 = None
    a2 = None
    for opt, arg in opts:
        if opt == "--papers":
            get_papers = True
        elif opt == "--a1":
            a1 = arg
        elif opt == "--a2":
            a2 = arg
        elif opt == "--dist":
            get_distance = True

    if not (get_distance or get_papers):
        sys.exit(0)

    if get_distance and (a1 is None) and (a2 is None):
        print "Please provide all the necessary options to compute distance" \
              "between two authors !"
        sys.exit(2)

    elif get_papers and a1 is None:
        print "Papers : Please provide an author name to search for !"
        sys.exit(2)

    main(a1, a2)


parse_options(sys.argv[1:])
