# coding = UTF-8

from wordcloud import WordCloud
from webapp import settings
#for tests
from tests import BaseTest
from authors import Researcher
from papers import Publication
from apis.elsevier import ResearcherQuery as RQ_elsevier
from apis.arxive import ARXIV_DATEFORMAT, ArxivQuery
from text_mining import Miner
import time

class WCgenerator(object):
    #small class that generates word clouds
    #in data

    def __init__(self,frequency_dis):
        self.frequency_dis = frequency_dis

    def generate(self,filename,stopwords = None):
        wc = WordCloud(stopwords=stopwords,width=1000,height=500,background_color='white')
        wc.generate_from_frequencies(self.frequency_dis)
        wc.to_file(filename)
        return True

class WordCloudTester(BaseTest):
    def run(self):
        start = time.time()
        self.passed = self.test_generate_word_cloud()
        elapsed = (time.time() - start)
        print ">>>>> Passed generate word cloud in " + str(elapsed) + " s"
        self.runs +=1
        return self.passed

    def test_generate_word_cloud(self):
        start = time.time()
        #michel = Researcher("Michel Habib")
        #serge = Researcher("Serge Abiteboul")
        arxiv_query1 = ArxivQuery(author="Michel Habib")
        arxiv_query2 = ArxivQuery(author="Xavier Leroy")
        # results_pierre = arxiv_query1.get_current_page()
        # results_michel = arxiv_query2.get_current_page()
        # elapsed = (time.time() - start)
        # print ">>>> Initializing Researchers in " + str(elapsed) + "s"
        start = time.time()
        #papers1 = michel.get_papers()
        #papers2 = serge.get_papers()
        papers1 = Publication.from_arxiv(arxiv_query1)
        papers2 = Publication.from_arxiv(arxiv_query2)
        elapsed = (time.time() - start)
        print ">>>> Getting papers in " + str(elapsed) + "s"
        start = time.time()
        #miner1 = Miner(papers1)
        #miner2 = Miner(papers2)
        miner3 = Miner(papers1,True)
        miner4 = Miner(papers2,True)
        #wcg = WCgenerator(miner1.get_frequency_dis_real())
        #wcg.generate(settings.BASE_DIR + "/coreutils/data/habib_with_common.png")
        #wcg2 = WCgenerator(miner1.merge_frequency_dis_real(miner2))
        #wcg2.generate(settings.BASE_DIR + "/coreutils/data/habib_abitoul_with_common.png")
        #wcg3 = WCgenerator(miner3.get_frequency_dis_real())
        #wcg3.generate(settings.BASE_DIR + "/coreutils/data/habib_without_common.png")
        wcg4 = WCgenerator(miner3.merge_frequency_dis_real(miner4))
        wcg4.generate(settings.BASE_DIR + "/coreutils/data/habib_leroy_without_common.png")
        print ">>>> Gnerating word cloud in " + str(elapsed) + "s"
        elapsed = (time.time() - start)
        return True

